# Contributing

Follow these instructions to build and run the project locally. 

Note, these are generic instructions that apply to (legacy) ASP.NET
Boilerplate template projects used by Visionet. You should have gotten the
following from the *README.md*.

- 1 backend .NET Core Web API repo.
- 1 frontend Angular repo.


## Table of Contents

- [Building](#building)
- [Running](#running)
- [Warnings](#warnings)
- [Making Changes](#making-changes)


## Building

### Development Prerequisites

The project is developed using the following programs. Install them if you
don't have them locally installed.

- Visual Studio 2017 or Visual Studio 2019 (Community Edition)
  - [Visual Studio 2017 (Community Edition)](https://visualstudio.microsoft.com/thank-you-downloading-visual-studio/?sku=Community&rel=15) 
  - [Visual Studio 2019 (Community Edition)](https://visualstudio.microsoft.com/thank-you-downloading-visual-studio/?sku=Community&rel=16)
- [.NET Core 2.1 SDK](https://www.microsoft.com/net/download/dotnet-core/2.1)
- [node](https://nodejs.org)
- [npm](https://npmjs.com)
- [yarn](https://yarnpkg.com/lang/en/docs/install/)

#### Yarn installation

After installing [npm](https://npmjs.com), run the following in the command line.

```sh
npm install yarn -g
```

### Building the backend solution

- Open *VDI.Demo.Web.sln* in Visual Studio.
- Press **[ CTRL ]+[ B ]**.
- In the **Solution Explorer**, look for **VDI.Demo.Web.Host**.
- In the Properties, open *launchSettings.json*.
- Make sure every row for `applicationUrl` is written like this.

```
	"applicationUrl": "http://localhost:22742/",
```

The frontend expects the backend to run on port 22742 while running locally.

### Creating the local database

- In the *VDI.Demo.Web.Host* project, open *appsettings.json*.
- In the connection strings section, find the following section.

```json
  "ConnectionStrings": {
    "Default": "Data Source=(localdb)\\MSSQLLOCALDB; Initial Catalog=VDIDemoDb; Integrated Security=true;"
  },
```

- Replace the *Initial Catalog* section with a unique name for your database.
- Click **Tools** > **NuGet Package Manager**.
- In the **Default project** dropdown, choose *src\VDI.Demo.EntityFrameworkCore*.
- Type in and run the following command in the **Package Manager Console**.

```
Update-Database -StartupProject VDI.Demo.Web.Host
```

### Building the frontend solution

- Go into the *Visionet.Template.FrontEnd/* directory.
- Open a command line and run the following command. Use a a high network
timeout due to some packages causing timeouts.

```sh
yarn install --network-timeout 1000000
```

## Running

### Running the backend

- Open Visual Studio and press **[ F5 ]**.

### Running the frontend

- Inside the folder containing the frontend *.sln* file, run the following.

```
yarn run start
```

- At the login screen, use the following credentials.
  - **Username**: admin
  - **Password**: 123qwe
- Change the password when prompted.


### Deploying to a dev server

> TODO: Complete this.

- Run the following command.

```
yarn run ng build --aot
```


## Warnings

### Do not change the project names or you'll break the license

**DO NOT** change the project names nor the assembly names of
the backend project because the license only works for that name. Changing it
may cause the project to not run at all.

### Try avoid inheriting AbpIntegratedTestBase<DemoTestModule> to avoid slow automated tests

Note that test classes that inherit `AbpIntegratedTestBase<DemoTestModule>`
will run `AbpBootstrapper.Initialize();` which can cause simple tests to take
more than 30 seconds. If you can avoid it, you SHOULD NOT inherit
`AbpIntegratedTestBase<DemoTestModule>`.


## Making Changes

### Updating the frontend to match the backend API

To have the Typescript files match the backend's API, do the folllowing steps.

- Run 'nswag/refresh.bat' to update 'src\shared\service-proxies\service-proxies.ts'.
   The service-proxies.ts file provide classes to call the backend. For example
   below is the code to get the audit logs from the backend's AuditLogService.

```typescript
        this._auditLogService.getAuditLogs(
            ...
        ).subscribe((result) => {
            this.primengDatatableHelper.totalRecordsCount = result.totalCount;
            this.primengDatatableHelper.records = result.items;
            this.primengDatatableHelper.hideLoadingIndicator();
        });
```

Note that if you added any new services, you still have to add them manually to
shared\service-proxies\service-proxy.module.ts. So something like:

```typescript
@NgModule({
    providers: [
        ApiServiceProxies.AuditLogServiceProxy,
        ApiServiceProxies.CachingServiceProxy,
        ApiServiceProxies.ChatServiceProxy,
        ApiServiceProxies.YOUR_NEW_SERVICE
...
```

### Creating new services

- Open Visual Studio
- Open the VDI.Demo.Application project.
- Search for AuditLogAppService.cs. You can use this class as an example of
  creating a new service.
- After creating the service in C#, run the backend project using F5. 
- Run 'nswag/refresh.bat' to update 'src\shared\service-proxies\service-proxies.ts'
  by reading the backend's API. The service-proxies.ts file provide classes to
  call the backend. For example below is the code to get the audit logs from
  the backend's AuditLogService.

```typescript
        this._auditLogService.getAuditLogs(
            ...
        ).subscribe((result) => {
            this.primengDatatableHelper.totalRecordsCount = result.totalCount;
            this.primengDatatableHelper.records = result.items;
            this.primengDatatableHelper.hideLoadingIndicator();
        });
```

6. Note that if you added any new services, you still have to add them manually
   to shared\service-proxies\service-proxy.module.ts. So something like:

```typescript
@NgModule({
    providers: [
        ApiServiceProxies.AuditLogServiceProxy,
        ApiServiceProxies.CachingServiceProxy,
        ApiServiceProxies.ChatServiceProxy,
        ApiServiceProxies.YOUR_NEW_SERVICE
...
```

### Creating new left-hand menu items

- Find '.\src\app\shared\layout\nav\app-navigation.service.ts'.
- Type something like the code block below, replacing the terms in all caps and
  underscores with the terms you want.

```typescript
        new AppMenuItem('NAME_OF_MENU_WITH_SUBMENUS', '', 'flaticon-NAME_OF_ICON', '', [
            new AppMenuItem('NAME_OF_MENU_WITH_HYPERLINK', '', 'flaticon-NAME_OF_ICON', 'RELATIVE_URL'),
        ]),
```

### Creating new shared Typescript files

Put shared frontend Typescript files in *src/app/main/share/*.

Put shared frontend Typescript files for specific pages in *src/app/main/share/NAME_OF_PAGE*.

### Creating dropdown components

Below is an example of a dropdown component that takes no parameters.

```ts
import { AfterContentInit, Component, ElementRef, forwardRef, ViewChild } from '@angular/core';
import { BaseDropDownListComponent, getCustomInputControlValueAccessor, templateString } from '@app/main/share/base-ddl.component';

import { RoleServiceProxy } from '@shared/service-proxies/service-proxies';

@Component({
    template: templateString,
    selector: 'example-ddl',
    providers: [getCustomInputControlValueAccessor(ExampleDdlComponent), BaseDropDownListComponent]
})
export class ExampleDdlComponent extends BaseDropDownListComponent implements AfterContentInit {
    @ViewChild('dropdownElement', {read: ElementRef}) el;

    listResult = this._roleService.getRoles(undefined);
    labelField = 'displayName';
    valueField = 'id';
    isLoading = true;

    constructor(
        private _roleService: RoleServiceProxy,
    ) {
        super();
    }

    ngAfterContentInit() {
        this.setDropdownElement(this.el);
    }
}
```

Below is an example of a dropdown that takes parameters.

```ts
import { AfterContentInit, Component, ElementRef, forwardRef, ViewChild, Input } from '@angular/core';
import { BaseDropDownListComponent, getCustomInputControlValueAccessor, templateString } from '@app/main/share/base-ddl.component';

import { LanguageServiceProxy } from '@shared/service-proxies/service-proxies';

@Component({
    template: templateString,
    selector: 'example-lang-ddl',
    providers: [getCustomInputControlValueAccessor(ExampleLangDdlComponent), BaseDropDownListComponent]
})
export class ExampleLangDdlComponent extends BaseDropDownListComponent implements AfterContentInit {
    @ViewChild('dropdownElement', {read: ElementRef}) el;
    @Input() input;

    listResult;
    labelField = 'displayName';
    valueField = 'name';
    isLoading = true;

    constructor(
        private _languageService: LanguageServiceProxy,
    ) {
        super();
    }

    ngAfterContentInit() {
        this.setDropdownElement(this.el);
    }

    retrieveByInput() {
        if(this.input != undefined && this.input != '')
        {
            // Adjust this line with your service
            this.listResult = this._languageService.getLanguageForEdit(this.input);
        }
        else {
            this.listResult = undefined;
            this.loadAllData();
        }
    }
}
```




## Common Issues

> The frontend is not listening to the correct port.

- Open *Visionet.Template.FrontEnd/nswag/service.config.nswag* with Notepad or Visual Studio.
- Replace the following line in with the location of the swagger service you 
are referencing.

```json
  "swaggerGenerator": {
    "fromSwagger": {
        "url": "http://YOUR_DOMAIN_NAME_OR_IP/swagger/v1/swagger.json",
        "output": null
    }
  },
 ```

- Open *Visionet.Template.FrontEnd/src/assets/appconfig.json* with Notepad or Visual Studio.
- Replace the following line in with the location of the swagger service you 
are referencing.

```json
    "remoteServiceBaseUrl": "http://YOUR_DOMAIN_NAME_OR_IP",
```

> Response status code does not indicate success: 500 (Internal Server Error).

Go to the backend project and check *App_Data/Logs/Logs.txt* and check for any issues
with http://localhost:22742/swagger/v1/swagger.json. Then fix them.


> npm run gets stuck on 90% chunk assets processing.

Try turning off source mapping. Source mapping maps typescript file lines to
the transpiled javascript file lines, making debugging easier. The problem is
that this can take a very long time.

If debugging is not a priority, try running the app using the following command.

```sh
yarn run ng serve --sourcemap=false --port=4200
```

Note, use ```yarn run``` instead of ```npm run``` because sometimes npm run
does not take the sourcemap or port parameters.


> Strange things with datepicker directive/which datepicker directive are we using.

We are **not** using the datepicker from ngx-bootstrap. We have our
own datepicker directive in app\shared\common\timing\date-picker.component.ts.

This may actually be a good thing because we have closer control and access
to the full features of JQuery's datepicker. As of writing this. ngx's datepicker
cannot handle "only displaying months".


> Dropdowns don't disappear on changing views.

This depends on where the dropdown is.

If the dropdown is in a grid, you're out of luck. You have to use the 
non-Angular bootstrap dropdown and add some code to manually delete
the dropdown. So say the dropdown button is supposed to go to
some page, replace the ```[routerLink]``` directive with something 
like this.

```
    goToPage(): void {
        this._router.navigate(['/app/target-page/', 'parameter']);

        let buggyDropdown = document.getElementsByClassName('dropdown-menu tether-element')[0];
        if (buggyDropdown != null) buggyDropdown.remove();
    }
```

Note that using the ```BsDropDownModule``` dropdown inside a
primefaces grid may cause the dropdown to not open at all.

Otherwise your dropdowns must use the ```BsDropDownModule``` 
from ```ngx-bootstrap```.  If the dropdown has something 
like ```data-toggle="dropdown"```, it's not using that module 
and is incorrect.

To use the module, make sure the following lines are on the 
xxx.module.ts file that serves your component.

```typescript
import { BsDropdownModule } from 'ngx-bootstrap';

@NgModule({
    imports: [
        BsDropdownModule.forRoot(),
...
```

Then replace the dropdown with something like below.

```html
            <div class="btn-group" normalizePosition dropdown>
                <button dropdownToggle
                        class="dropdown-toggle btn btn-sm btn-primary"
                        aria-controls="dropdown-basic">
                    <i class="fa fa-cog"></i><span class="caret"></span> {{l("Actions")}}
                </button>
                <ul *dropdownMenu class="dropdown-menu">
                    <li role="menuitem">
                        <a class="dropdown-item">{{l('Details')}}</a>
                    </li>
                </ul>
            </div>
```

> Error: "ExpressionChangedAfterItHasBeenCheckedError: Expression has changed after it was checked.

> TODO: Find a fix for this.


> "Found the synthetic property @routerTransition. Please include either "BrowserAnimationsModule" or "NoopAnimationsModule" in your application.",

If both `BrowserAnimationmodule` and `NoopAnimationModule` are already in 
the *root.module.ts* file, make sure that the page's component.ts file 
has the following lines.

```typescript
import { appModuleAnimation } from '@shared/animations/routerTransition';

@Component({
    ...
    animations: [appModuleAnimation()]
})
```


> http://BACKEND/AbpUserConfiguration/GetAll returns a 500 status error

Try clearing the cookies in your browser.


## Terminology (IND)

- **Directive**: Suatu class terpisah yang digunakan untuk menerapkan sesuatu
ke komponen yang menggunakannya. Contoh: datepicker, masking phone number
input, format currency.
- **Shared component**: Suatu komponen yang dipecah menjadi bagian sendiri
agar dapat digunakan dibeberapa tempat sekaligus (contoh, dropdown negara,
form sign up, pesan error validasi form).
