# Terminology

> Note: Copies of this document should be verbatim copies of the one in the
> Hydra project.


## Unsorted terms

- **AS**: Account Statement.
- **BAST**: Berita Acara Serah Terima.
- **Block**: A city block. A collection of clusters (i.e. apartment towers).
- **Cluster**: A tower. A collection of apartment units.
- **DocNo**: The unique identifier of a document, usually formatted like
   *00012/DOCTYPE/MM/YYYY* where the first number indicates that it was the 12th
   document printed in month *MM* year *YYYY*. 
   
There are cases where there are suffixes in the DocNo, such as

> 000665/PPPU-MSU/03/2018/LGWCP

> 000608/PSAS-MSU/SI/III/2018

The unique identifier remains the �PPPU� segment, which should be stored in `MS_Document`.

- **DP**: Down payment.
- **FSD**: Functional Specifications Document.
- **MSU**: Mahkota Sentosa Utama, an Indonesian construction company (MSU is
  its company code in the `MS_Company` DB table).
- **No**.: Important note, in both English and Indonesian, No. is an
   abbreviation for number. But in the database context, things with the suffix
   No. means alphanumeric. This is definitely an error when choosing the name, but
   it may be too late to change it).
- **PS**: Shorthand for the word "Personal". For example, `psCode` means "personalCode".
- **PSAS**: Property Sales Admin Service (A department)
- **PPPU**: The Penegasan Persetujuan dan Pemesanan Unit division. The initials
   PPPU are also used for a type of document related to this division.
- **PPPU Document**: A document related to the Penegasan Persetujuan dan Pemesanan Unit division.
- **Unit**: An apartment unit.


## Item codes

- **01**: Land
- **02**: Building
- **GO**: Renovations

Last updated 2019-09-13.