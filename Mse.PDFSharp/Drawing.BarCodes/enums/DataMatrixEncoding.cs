namespace Mse.PDFSharp.Drawing.BarCodes
{
    /// <summary>
    /// docDaSt
    /// </summary>
    public enum DataMatrixEncoding
    {
        /// <summary>
        /// docDaSt
        /// </summary>
        Ascii,

        /// <summary>
        /// docDaSt
        /// </summary>
        C40,

        /// <summary>
        /// docDaSt
        /// </summary>
        Text,

        /// <summary>
        /// docDaSt
        /// </summary>
        X12,

        /// <summary>
        /// docDaSt
        /// </summary>
        // ReSharper disable once InconsistentNaming
        EDIFACT,

        /// <summary>
        /// docDaSt
        /// </summary>
        Base256
    }
}
