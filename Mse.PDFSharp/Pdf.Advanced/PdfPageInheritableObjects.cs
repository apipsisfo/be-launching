using System;

namespace Mse.PDFSharp.Pdf.Advanced
{
    /// <summary>
    /// Represents a PDF page object.
    /// </summary>
    internal class PdfPageInheritableObjects : PdfDictionary
    {
        public PdfPageInheritableObjects()
        { }

        // TODO Inheritable Resources not yet supported

        /// <summary>
        /// 
        /// </summary>
        public PdfRectangle MediaBox
        {
            get { return _mediaBox; }
            set { _mediaBox = value; }
        }
        PdfRectangle _mediaBox;

        public PdfRectangle CropBox
        {
            get { return _cropBox; }
            set { _cropBox = value; }
        }
        PdfRectangle _cropBox;

        public int Rotate
        {
            get { return _rotate; }
            set
            {
                if (value % 90 != 0)
                    throw new ArgumentException("Rotate", "The value must be a multiple of 90.");
                _rotate = value;
            }
        }
        int _rotate;
    }
}
