namespace Mse.PDFSharp
{
    /// <summary>
    /// Base namespace of PdfSharpCore. Most classes are implemented in nested namespaces like e. g. PdfSharpCore.Pdf.
    /// </summary>
    /// <seealso cref="Didstopia.PDFSharp.Pdf"></seealso>
    [System.Runtime.CompilerServices.CompilerGenerated]
    internal class NamespaceDoc { }

    /// <summary>
    /// Specifies the orientation of a page.
    /// </summary>
    public enum PageOrientation
    {
        /// <summary>
        /// The default page orientation.
        /// </summary>
        Portrait,

        /// <summary>
        /// The width and height of the page are reversed.
        /// </summary>
        Landscape,
    }
}
