# be-dotnetcore

> The backend for the Meikarta web app.


## Table of Contents

- [Background](#background)
- [Contributing](#contributing)


## Background

This is the backend for a single-page application serving
[Meikarta](http://meikarta.com/), a planned city project. 

It's an ASP.NET Core Web API using the 
[ASP.NET Boilerplate by Volosoft](https://www.aspnetzero.com) 
library. Documentation on ASP.NET Boilerplate is 
[here](https://www.aspnetzero.com/Documents).

### Frontend

Below are the frontend(s) for this project.

- **Meikarta Online Booking Frontend**: https://gitlab.com/neogeekscamp/online-booking-web
- **Meikarta Workflow Frontend**: https://gitlab.com/neogeekscamp/fe-angular-workflow-msu


## Contributing

New devs should read the [CONTRIBUTING.md](CONTRIBUTING.md) file.

