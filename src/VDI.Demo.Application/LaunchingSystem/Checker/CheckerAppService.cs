﻿using System;
using System.Collections.Generic;
using System.Text;
using VDI.Demo.EntityFrameworkCore.LaunchingSystem;
using VDI.Demo.EntityFrameworkCore.Personal;
using VDI.Demo.EntityFrameworkCore.PropertySystem;
using VDI.Demo.LaunchingSystem.Checker.Dto;
using System.Linq;

namespace VDI.Demo.LaunchingSystem.Checker
{
    public class CheckerAppService : DemoAppServiceBase , ICheckerAppService
    {
        private readonly LaunchingSystemDbContext _launchingSystemDbContext;
        private readonly PersonalsNewDbContext _personalsNewDbContext;
        private readonly PropertySystemDbContext _propertySystemDbContext;

        public CheckerAppService(
                LaunchingSystemDbContext launchingSystemDbContext,
                PersonalsNewDbContext personalsNewDbContext,
                PropertySystemDbContext propertySystemDbContext
            )
        {
            _launchingSystemDbContext = launchingSystemDbContext;
            _personalsNewDbContext = personalsNewDbContext;
            _propertySystemDbContext = propertySystemDbContext;
        }

        public void CheckPost(QueueDto data)
        {
            var dt = _launchingSystemDbContext.TR_PPQueue.Where(ss => ss.ppNo == data.ppNo && ss.queueNo == data.queueNo).FirstOrDefault();
            long ID = dt.Id;

            try
            {
                TR_PPQueue toUpdate = _launchingSystemDbContext.TR_PPQueue.Find(ID);
                switch (data.queueStatus)
                {
                    case 1:
                        toUpdate.queueStatus = PPQueueStatus.Processed;
                        break;
                    case 2:
                        toUpdate.queueStatus = PPQueueStatus.Cancelled;
                        break;
                    case 3:
                        toUpdate.queueStatus = PPQueueStatus.Done;
                        break;
                    case 4:
                        toUpdate.queueStatus = PPQueueStatus.Unchecked;
                        break;
                }

                _launchingSystemDbContext.SaveChanges();
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
