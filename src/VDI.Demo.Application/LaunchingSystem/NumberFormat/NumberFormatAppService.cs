﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.UI;
using VDI.Demo.EntityFrameworkCore.LaunchingSystem;

namespace VDI.Demo.LaunchingSystem.NumberFormat
{
    public class NumberFormatAppService : DemoAppServiceBase, INumberFormatAppService
    {
        private readonly LaunchingSystemDbContext _dbContext;

        public NumberFormatAppService(
         LaunchingSystemDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<string> GetGenerateNo(string type)
        {
            try
            {
                if (String.IsNullOrEmpty(type))
                    throw new Exception("Please fill type parameter");

                var data = _dbContext.MS_NumberFormat.Where(w => w.formatType.ToLower() == type.ToLower()).LastOrDefault();

                if (data == null)
                    throw new Exception("Can not generate number format. Type not found");

                string result = "";
                data.lastNo = data.lastNo + 1;
                string middleText = data.lastNo.ToString().PadLeft(data.countDigitNo, '0');

                result = data.prefix + middleText + data.postfix;

                _dbContext.MS_NumberFormat.Update(data);
                await _dbContext.SaveChangesAsync();

                return result;
            }
            catch (Exception ex)
            {
                Logger.ErrorFormat($"GenerateNo() error:{Environment.NewLine}" +
                                   $"type = {type}{Environment.NewLine}" +
                                   $"ErrorReason = {ex.Message}");

                throw new UserFriendlyException($"Error : {ex.Message}");
            }
        }
    }
}
