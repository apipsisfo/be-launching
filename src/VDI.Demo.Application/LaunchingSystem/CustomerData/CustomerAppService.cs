﻿using System;
using System.Collections.Generic;
using System.Text;
using VDI.Demo.EntityFrameworkCore.LaunchingSystem;
using VDI.Demo.EntityFrameworkCore.Personal;
using VDI.Demo.EntityFrameworkCore.PropertySystem;
using System.Linq;
using VDI.Demo.LaunchingSystem.Customer;
using VDI.Demo.LaunchingSystem.Customer.Dto;
using Abp.UI;
using VDI.Demo.SqlExecuter;
using System.Threading.Tasks;
using VDI.Demo.Personal.Occupation.Dto;

namespace VDI.Demo.LaunchingSystem.CustomerData
{
    public class CustomerAppService : DemoAppServiceBase, ICustomerAppService
    {
        private readonly LaunchingSystemDbContext _launchingSystemDbContext;
        private readonly PersonalsNewDbContext _personalsNewDbContext;
        private readonly PropertySystemDbContext _propertySystemDbContext;
        private readonly ISqlExecuter _sqlExecuter;

        public CustomerAppService(
                LaunchingSystemDbContext launchingSystemDbContext,
                PersonalsNewDbContext personalsNewDbContext,
                PropertySystemDbContext propertySystemDbContext,
                ISqlExecuter sqlExecuter
            )
        {
            _launchingSystemDbContext = launchingSystemDbContext;
            _personalsNewDbContext = personalsNewDbContext;
            _propertySystemDbContext = propertySystemDbContext;
            _sqlExecuter = sqlExecuter;
        }

        public CustomerDto GetListCustomer(string NUP)
        {
            CustomerDto Model = new CustomerDto();
            List<NUPList> listNUP = new List<NUPList>();
            List<DocList> listsDoc = new List<DocList>();

            var SP_GetDataUnitType = "exec SP_GetDataUnitType @PPNo, @row";
            var ms_parameter = _propertySystemDbContext.MS_Parameter.Where(x => x.code == "FORDG" && x.isActive == true).FirstOrDefault();
            int digitsNIk = int.Parse(ms_parameter.value); //4 digits nik from right
            try
            {
                if (String.IsNullOrEmpty(NUP))
                    throw new Exception("Please fill type parameter");

                var psCode = _propertySystemDbContext.TR_PriorityPass.Where(ss => ss.PPNo == NUP).FirstOrDefault();

                if (psCode == null)
                    throw new Exception("PPNo not found");

                var allNUP = (from a in _propertySystemDbContext.TR_PriorityPass.Where(ss => ss.psCode == psCode.psCode && ss.LK_PPStatus.PPStatusName.ToUpper().Contains("NEW") && ss.batchID == psCode.batchID && ss.batchSeq == psCode.batchSeq)
                              join b in _propertySystemDbContext.MS_BatchEntry on a.batchID equals b.Id
                              join c in _propertySystemDbContext.MS_ProjectInfo on b.projectInfoID equals c.Id
                              join d in _propertySystemDbContext.MS_Project.Where(ss => ss.projectCode.ToUpper().Contains("RHK")) on c.projectID equals d.Id
                              select new
                              {
                                  projectID = d.Id,
                                  projectName = d.projectName,
                                  ppNo = a.PPNo,
                              }
                           ).ToList();

                if (allNUP.Count == 0)
                    throw new Exception("PPNo not found");

                var dtMember = (from a in _personalsNewDbContext.PERSONALS_MEMBER.Where(ss => ss.memberCode == psCode.memberCode)
                                join b in _personalsNewDbContext.PERSONAL on a.psCode equals b.psCode
                                select b.name).FirstOrDefault();

                var dtPersonal = (from a in _personalsNewDbContext.PERSONAL.Where(ss => ss.psCode == psCode.psCode)
                                  join b in _personalsNewDbContext.TR_ID.DefaultIfEmpty() on a.psCode equals b.psCode
                                  join c in _personalsNewDbContext.TR_Phone.DefaultIfEmpty() on a.psCode equals c.psCode
                                  join d in _personalsNewDbContext.TR_Email.DefaultIfEmpty() on a.psCode equals d.psCode
                                  join e in _personalsNewDbContext.TR_Address.DefaultIfEmpty() on a.psCode equals e.psCode
                                  select new CustomerDto
                                  {
                                      PsCode = a.psCode,
                                      Name = a.name,
                                      NIK = b.idNo,
                                      PIN = b.idNo.Length >= 4 ? b.idNo.Substring(b.idNo.Length - digitsNIk) : b.idNo,
                                      Address = e.address,
                                      PhoneNo = c.number,
                                      Email = d.email
                                  }).FirstOrDefault();

                Model.ProjectID = allNUP.FirstOrDefault().projectID;
                Model.ProjectName = allNUP.FirstOrDefault().projectName;
                Model.PsCode = dtPersonal.PsCode;
                Model.Name = dtPersonal.Name;
                Model.NIK = dtPersonal.NIK;
                Model.PIN = dtPersonal.PIN;
                Model.Address = dtPersonal.Address;
                Model.PhoneNo = dtPersonal.PhoneNo;
                Model.Email = dtPersonal.Email;

                var documentOption = new List<string>
                {
                    "KTP",
                    "NPWP"
                };

                var TR_Document = _personalsNewDbContext.TR_Document.Where(ss => ss.psCode == psCode.psCode).ToList();

                int cekDoc = 0;
                #region loopDocument
                foreach (var docs in documentOption)
                {
                    DocList doc = new DocList();
                    var getData = TR_Document.Where(ss => ss.documentType == docs).FirstOrDefault();

                    if (docs == "KTP" && getData == null)
                    {
                        getData = TR_Document.Where(ss => ss.documentType == "KITAS").FirstOrDefault();
                        doc.DocumentType = getData.documentType;
                        doc.DocumentPICType = getData.documentPicType;
                        doc.Base64Image = Convert.ToBase64String(getData.documentBinary);

                        cekDoc += 1;
                    }
                    else
                    {
                        if (getData != null)
                        {
                            doc.DocumentType = getData.documentType;
                            doc.DocumentPICType = getData.documentPicType;
                            doc.Base64Image = Convert.ToBase64String(getData.documentBinary);

                            cekDoc += 1;
                        }
                        else
                        {
                            continue;
                        }
                    }

                    listsDoc.Add(doc);
                }
                #endregion

                Model.IsCompleteDoc = cekDoc == 2 ? true : false;

                #region loopNUP
                for (int i = 0; i < allNUP.Count; i++)
                {
                    NUPList tempModel = new NUPList();
                    var PPNo = allNUP[i].ppNo;
                    var row = i + 1;
                    var detailUnitType = _sqlExecuter.GetFromPropertySystem<NUPList>
                                                     (SP_GetDataUnitType, new { PPNo, row },
                                                     System.Data.CommandType.StoredProcedure).FirstOrDefault();

                    if (detailUnitType != null)
                    {
                        tempModel.No = row;
                        tempModel.PPNo = detailUnitType.PPNo;
                        tempModel.PIN = Model.PIN;
                        tempModel.ppOrderID = detailUnitType.ppOrderID;
                        tempModel.termCode = detailUnitType.termCode;
                        tempModel.schemaCode = detailUnitType.schemaCode;
                        tempModel.isPriorityLine = detailUnitType.isPriorityLine;
                        tempModel.detailID = detailUnitType.detailID;
                        tempModel.detailName = detailUnitType.detailName;
                        tempModel.memberCode = psCode.psCode;
                        tempModel.memberName = dtMember != null ? dtMember : "";
                    }

                    listNUP.Add(tempModel);
                }
                #endregion

                Model.ListDoc = listsDoc;
                Model.ListNUP = listNUP;

            }
            catch (Exception ex)
            {
                Logger.ErrorFormat($"ListCustomer() error:{Environment.NewLine}" +
                                                 $"type = {NUP}{Environment.NewLine}" +
                                                 $"ErrorReason = {ex.Message}");

                throw new UserFriendlyException($"Error : {ex.Message}");
            }

            return Model;
        }

        public BatchListDto GetBatchDropdown()
        {
            BatchListDto listsBacth = new BatchListDto();
            List<BatchDto> data = new List<BatchDto>();
            var dataBatch = _launchingSystemDbContext.MS_Batch.ToList();

            foreach (var item in dataBatch)
            {
                BatchDto Model = new BatchDto();
                Model.ID = item.Id;
                Model.BatchName = item.batchName;
                Model.startTime = item.startTime.ToString("HH:mm");
                Model.endTime = item.endTime.ToString("HH:mm");
                Model.BatchTime = Model.startTime + " - " + Model.endTime;
                data.Add(Model);
            }

            listsBacth.BatchLists = data;
            return listsBacth;
        }

        public ResultMesaggeDto RegisterCustomer(CustomerPostDto input)
        {
            try
            {
                return new ResultMesaggeDto { result = true, message = "Register Customer Succes" };
            }
            catch (Exception)
            {
                throw;
            }

        }

        public async Task<List<OccupationDropdownDto>> GetDropdownOccupation()
        {
            return await Task.Factory.StartNew(() =>
            {
                var result = _personalsNewDbContext.MS_Occupation.Select(s => new OccupationDropdownDto
                {
                    occDesc = s.occDesc,
                    occID = s.occID
                }).ToList();

                return result;
            });
        }
    }
}
