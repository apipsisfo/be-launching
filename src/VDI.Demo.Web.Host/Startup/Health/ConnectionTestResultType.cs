﻿namespace VDI.Demo.Web.Startup.Health
{
    public enum ConnectionTestResultType
    {
        Success,
        TableMissing,
        ConnectionFailed
    }
}
