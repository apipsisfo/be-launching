﻿namespace VDI.Demo.Web.Startup.Health
{
    public class ConnectionTestResultDto
    {
        public string DbContextName { get; set; }
        public ConnectionTestResultType ConnectionTestResultType { get; set; }
    }
}
