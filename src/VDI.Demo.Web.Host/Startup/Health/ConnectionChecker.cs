﻿using Dapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace VDI.Demo.Web.Startup.Health
{
    /// <summary>
    /// Responsible for making sure connection strings are valid and point to
    /// the correct databases by running a test SQL query and checking if
    /// certain tables exist.
    /// </summary>
    public class ConnectionChecker
    {
        private readonly IServiceProvider _serviceProvider;
        public IList<ConnectionTestResultDto> ConnectionTestResults { get; private set; }

        public ConnectionChecker(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
            ConnectionTestResults = new List<ConnectionTestResultDto>();
        }

        public void CheckDbAndTableAccessible<TDbContext>(Expression<Func<TDbContext, object>> getTableName) where TDbContext : DbContext
        {
            var memberExpression = getTableName.Body as MemberExpression;

            var propertyInfo = memberExpression.Member as PropertyInfo;

            using (var dbContext = _serviceProvider.GetService<TDbContext>())
            using (var connection = dbContext.Database.GetDbConnection())
            {
                try
                {
                    var results = connection.Query(
                        $"SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = @TableName",
                        new
                        {
                            TableName = propertyInfo.Name
                        }
                    );

                    if (results.Any())
                    {
                        ConnectionTestResults.Add(new ConnectionTestResultDto
                        {
                            DbContextName = typeof(TDbContext).Name,
                            ConnectionTestResultType = ConnectionTestResultType.Success
                        });
                    }
                    else
                    {
                        ConnectionTestResults.Add(new ConnectionTestResultDto
                        {
                            DbContextName = typeof(TDbContext).Name,
                            ConnectionTestResultType = ConnectionTestResultType.TableMissing
                        });
                    }
                }
                catch (Exception)
                {
                    ConnectionTestResults.Add(new ConnectionTestResultDto
                    {
                        DbContextName = typeof(TDbContext).Name,
                        ConnectionTestResultType = ConnectionTestResultType.ConnectionFailed
                    });
                }
            }
        }
    }
}
