﻿namespace VDI.Demo.Web.Startup
{
    /// <summary>
    /// A DTO that contains every connection string used in the app.
    /// </summary>
    public class StartupConnectionStrings
    {
        public string DecryptedCoreConnectionString { get; set; }
        public string DecryptedNewCommConnectionString { get; set; }
        public string DecryptedPropertySystemConnectionString { get; set; }
        public string DecryptedPersonalConnectionString { get; set; }
        public string DecryptedAccountingConnectionString { get; set; }
        public string DecryptedTaxConnectionString { get; set; }
        public string DecryptedPPOnlineConnectionString { get; set; }
        public string DecryptedOracleConnectionString { get; set; }
        public string DecryptedSIMExecutiveConnectionString { get; set; }
        public string DecryptedHumanResourcesConnectionString { get; set; }
        public string DecryptedSmsCampaignConnectionString { get; set; }
        public string DecryptedWorkflowConnectionString { get; set; }
        public string DecryptedDiagramaticConnectionString { get; set; }
    }
}
