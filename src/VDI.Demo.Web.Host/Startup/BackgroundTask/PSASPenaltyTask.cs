﻿using Abp.Dependency;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VDI.Demo.PSAS.Penalties;
using VDI.Demo.PSAS.SuratPeringatans;

namespace VDI.Demo.Web.Startup.BackgroundTask
{
    public class PSASPenaltyTask
    {
        public static void Run()
        {
            var PSASPenaltyAppService = IocManager.Instance.Resolve<IPSASPenaltyAppService>();
            PSASPenaltyAppService.CreatePenaltyScheduler();
            PSASPenaltyAppService.CreatePaymentPenaltyWaive();
        }
    }
}
