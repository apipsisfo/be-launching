﻿using Abp.Dependency;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VDI.Demo.PSAS.LegalDocument.MappingTemplate;

namespace VDI.Demo.Web.Startup.BackgroundTask
{
    public class LegalDocumentMappingTemplateTask
    {
        public static void Run()
        {
            var mappingTemplateAppService = IocManager.Instance.Resolve<IPSASMappingTemplateAppService>();
            mappingTemplateAppService.SchedulerUpdateDataExpired();
        }
    }
}
