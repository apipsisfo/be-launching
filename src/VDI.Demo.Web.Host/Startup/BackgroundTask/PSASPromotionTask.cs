﻿using Abp.Dependency;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VDI.Demo.PSAS.Notification.Promotion;

namespace VDI.Demo.Web.Startup.BackgroundTask
{
    public class PSASPromotionTask
    {
        public static void Run()
        {
            var promotionAppService = IocManager.Instance.Resolve<IPromotionAppService>();
            promotionAppService.UpdateIsActiveExpiredPromotionScheduller();         
        }
    }
}
