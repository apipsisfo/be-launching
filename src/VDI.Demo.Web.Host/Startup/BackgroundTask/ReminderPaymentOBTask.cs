﻿using Abp.Dependency;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VDI.Demo.OnlineBooking.Transaction;

namespace VDI.Demo.Web.Startup.BackgroundTask
{
    public class ReminderPaymentOBTask
    {
        public static void Run()
        {
            var transactionAppService = IocManager.Instance.Resolve<ITransactionAppService>();
            transactionAppService.SchedulerStatusOrderExpired();
            transactionAppService.SchedulerBookingReminder();
        }
    }
}
