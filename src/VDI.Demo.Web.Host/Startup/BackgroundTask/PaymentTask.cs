﻿using Abp.Dependency;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VDI.Demo.Payment.InputPayment;

namespace VDI.Demo.Web.Startup.BackgroundTask
{
    public class PaymentTask
    {
        public static void Run()
        {
            var InputPaymentAppService = IocManager.Instance.Resolve<IInputPaymentAppService>();
            InputPaymentAppService.SendEmailORScheduler();
        }
    }
}
