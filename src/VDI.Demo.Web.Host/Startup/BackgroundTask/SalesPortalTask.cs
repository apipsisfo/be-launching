﻿using Abp.Dependency;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VDI.Demo.SalesPortal.TaskList;

namespace VDI.Demo.Web.Startup.BackgroundTask
{
    public class SalesPortalTask
    {
        public static void Run() {
            var taskListAppService = IocManager.Instance.Resolve<TaskListSPAppService>();
            taskListAppService.SchedulerTaskList();
        }
    }
}
