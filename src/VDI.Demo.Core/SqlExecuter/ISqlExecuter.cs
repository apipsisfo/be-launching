﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace VDI.Demo.SqlExecuter
{
    public interface ISqlExecuter
    {
        int ExecutePropertySystem(string sql, params object[] parameters);
        IReadOnlyList<T> GetFromEngin3<T>(string sql, object parameters = null, CommandType? commandType = null);
        IReadOnlyList<T> GetFromPropertySystem<T>(string sql, object parameters = null, CommandType? commandType = null);
    }
}
