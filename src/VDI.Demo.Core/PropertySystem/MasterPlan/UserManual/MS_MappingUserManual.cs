﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VDI.Demo.PropertySystemDB.MasterPlan.UserManual
{
    [Table("MS_MappingUserManual")]
    public class MS_MappingUserManual : AuditedEntity
    {
        [Required]
        [StringLength(100)]
        public string AppName { get; set; }

        [Required]
        [StringLength(100)]
        public string ManualName { get; set; }

        [Required]
        public string ManualLink { get; set; }

        [Required]
        public bool isActive { get; set; }
    }
}
