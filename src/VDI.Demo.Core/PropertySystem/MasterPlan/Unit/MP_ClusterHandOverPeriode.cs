﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VDI.Demo.PropertySystemDB.MasterPlan.Unit
{
    [Table("MP_ClusterHandOverPeriode")]
    public class MP_ClusterHandOverPeriode : AuditedEntity
    {
        public int entityID { get; set; }

        public DateTime handOverStart { get; set; }

        public DateTime handOverEnd { get; set; }

        public string handOverDue { get; set; }

        public string graceDue { get; set; }

        [ForeignKey("MS_Cluster")]
        public int clusterID { get; set; }
        public virtual MS_Cluster MS_Cluster { get; set; }
    }
}
