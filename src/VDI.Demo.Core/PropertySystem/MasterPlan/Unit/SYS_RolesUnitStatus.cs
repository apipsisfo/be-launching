﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VDI.Demo.PropertySystemDB.MasterPlan.Unit
{
    [Table("SYS_RolesUnitStatus")]
    public class SYS_RolesUnitStatus : AuditedEntity
    {
        public int entityID { get; set; }

        public int rolesID { get; set; }

        [StringLength(100)]
        public string unitStatusBefore { get; set; }

        [StringLength(100)]
        public string unitStatusAfter { get; set; }
    }
}
