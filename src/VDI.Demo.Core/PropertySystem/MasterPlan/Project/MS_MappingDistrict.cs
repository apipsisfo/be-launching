﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using VDI.Demo.PropertySystemDB.MasterPlan.Unit;

namespace VDI.Demo.PropertySystemDB.MasterPlan.Project
{
    [Table("MS_MappingDistrict")]
    public class MS_MappingDistrict : AuditedEntity
    {
        [Required]
        [StringLength(15)]
        public string DistrictCode { get; set; }
        [Required]
        [StringLength(100)]
        public string DistrictName { get; set; }
        [Required]
        [StringLength(25)]
        public string blok { get; set; }

        [ForeignKey("MS_Cluster")]
        public int clusterID { get; set; }
        public virtual MS_Cluster MS_Cluster { get; set; }

        [ForeignKey("MS_Project")]
        public int projectID { get; set; }
        public virtual MS_Project MS_Project { get; set; }

        [Required]
        [StringLength(25)]
        public string typeCluster { get; set; }
    }
}
