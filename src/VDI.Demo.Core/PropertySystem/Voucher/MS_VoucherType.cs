﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using VDI.Demo.PropertySystemDB.MasterPlan.Project;

namespace VDI.Demo.PropertySystemDB.Voucher
{
    public class MS_VoucherType : Entity<string>
    {
        [NotMapped]
        public override string Id
        {
            get
            {
                return projectID +
                    "-" + voucherTypeCode;
            }
            set { /* nothing */ }
        }

        [Key]
        [Column(Order = 0)]

        [ForeignKey("MS_Project")]
        public int projectID { get; set; }
        public virtual MS_Project MS_Project { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(6)]
        public string voucherTypeCode { get; set; }

        [Required]
        [StringLength(255)]
        public string voucherTypeName { get; set; }

        public DateTime inputTime { get; set; }

        [Required]
        public string inputUN { get; set; }

        public int? expiredDate { get; set; }
    }
}
