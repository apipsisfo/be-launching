﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VDI.Demo.PropertySystemDB.Voucher
{
    public class MS_Location : AuditedEntity
    {

        [NotMapped]
        public override int Id { get { return IdLocation; } set { IdLocation = value; } }

        public int IdLocation { get; set; }

        [Required]
        [StringLength(50)]
        public string LocationName { get; set; }

        [Required]
        [StringLength(50)]
        public string PicNam { get; set; }

        [Required]
        [StringLength(100)]
        public string PicEmail { get; set; }

        [Required]
        [StringLength(50)]
        public string PicTelfon { get; set; }
    }
}
