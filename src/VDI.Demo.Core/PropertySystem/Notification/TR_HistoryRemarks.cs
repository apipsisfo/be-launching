﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VDI.Demo.PropertySystemDB.Notification
{
    [Table("TR_HistoryRemarks")]
    public class TR_HistoryRemarks : AuditedEntity
    {
        [ForeignKey("TR_Notification")]
        public int notificationID { get; set; }
        public virtual TR_Notification TR_Notification { get; set; }

        [Required]
        [StringLength(100)]
        public string subject { get; set; }

        [ForeignKey("TR_NotificationContactType")]
        public int notificationContactTypeID { get; set; }
        public virtual TR_NotificationContactType TR_NotificationContactType { get; set; }

        [Required]
        [StringLength(500)]
        public string destinationContact { get; set; }

        [Required]
        [StringLength(100)]
        public string remarks { get; set; }

        public bool isActive { get; set; }

        [StringLength(50)]
        public string CreatorUserName { get; set; }

        [StringLength(50)]
        public string LastModifierUserName { get; set; }

    }
}
