﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using VDI.Demo.PropertySystemDB.MasterPlan.Project;

namespace VDI.Demo.PropertySystemDB.Notification
{
    [Table("TR_TemplateNotification")]
    public class TR_TemplateNotification : AuditedEntity
    {
        [ForeignKey("MS_Project")]
        public int projectID { get; set; }
        public virtual MS_Project MS_Project { get; set; }

        [ForeignKey("MS_NotificationType")]
        public int notifTypeID { get; set; }
        public virtual MS_NotificationType MS_NotificationType { get; set; }

        [Required]
        [StringLength(200)]
        public string subject { get; set; }

        [Required]
        [StringLength(50)]
        public string messageName { get; set; }

        [Required]
        public string messageUrl { get; set; }

        public bool isModified { get; set; }

        public bool status { get; set; }

        public bool isActive { get; set; }

        [StringLength(50)]
        public string CreatorUserName { get; set; }

        [StringLength(50)]
        public string LastModifierUserName { get; set; }

        public ICollection<TR_Variable> TR_Variable { get; set; }
    }
}
