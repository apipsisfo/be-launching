﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VDI.Demo.PropertySystemDB.Notification
{
    [Table("MS_NotificationType")]
    public class MS_NotificationType  : AuditedEntity
    {       
        [Required]
        [StringLength(50)]
        public string notifTypeCode { get; set; }

        [Required]
        [StringLength(250)]
        public string notifTypeDesc { get; set; }

        [StringLength(50)]
        public string parentType { get; set; }

        public bool canBeDeleted { get; set; }

        [Required]
        [StringLength(500)]
        public string notifSubject { get; set; }

        [Column("modifTime")]
        public override DateTime? LastModificationTime { get; set; }

        [Column("modifUN")]
        public override long? LastModifierUserId { get; set; }

        [Column("inputTime")]
        public override DateTime CreationTime { get; set; }

        [Column("inputUN")]
        public override long? CreatorUserId { get; set; }

        public ICollection<MS_MessageTemplate> MS_MessageTemplate { get; set; }

        public ICollection<TR_PromotionNotification> TR_PromotionNotification { get; set; }

        public ICollection<TR_TemplateNotification> TR_TemplateNotification { get; set; }
    }
}
