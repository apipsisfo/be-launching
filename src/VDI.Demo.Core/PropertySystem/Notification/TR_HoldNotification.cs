﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using VDI.Demo.PropertySystemDB.MasterPlan.Project;

namespace VDI.Demo.PropertySystemDB.Notification
{
    [Table("TR_HoldNotification")]
    public class TR_HoldNotification : AuditedEntity
    {
        //[Required]
        [StringLength(20)]
        public string bookCode { get; set; }

        //[Required]
        [StringLength(8)]
        public string psCode { get; set; }

        public bool isHold { get; set; }

        public int projectId { get; set; }

        public int clusterId { get; set; }

        [Required]
        [StringLength(5)]
        public string notifTypeCode { get; set; }

        public int notifTypeID { get; set; }

        public string messagename { get; set; }
       
    }
}
