﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VDI.Demo.PropertySystemDB.Notification
{
    [Table("TR_Notification")]
    public class TR_Notification : AuditedEntity
    {
        [Required]
        [StringLength(5)]
        public string notifTypeCode { get; set; }

        [Required]
        [StringLength(50)]
        public string appName { get; set; }

        [StringLength(100)]
        public string notifSubject { get; set; }

        [Required]
        [StringLength(20)]
        public string bookCode { get; set; }

        [Required]
        [StringLength(100)]
        public string descContact { get; set; }

        public string message { get; set; }

        public DateTime executionTime { get; set; }

        public string attachment { get; set; }

        [Column("modifTime")]
        public override DateTime? LastModificationTime { get; set; }

        [Column("modifUN")]
        public override long? LastModifierUserId { get; set; }

        [Column("inputTime")]
        public override DateTime CreationTime { get; set; }

        [Column("inputUN")]
        public override long? CreatorUserId { get; set; }

        public ICollection<TR_HistoryRemarks> TR_HistoryRemarks { get; set; }






    }
}
