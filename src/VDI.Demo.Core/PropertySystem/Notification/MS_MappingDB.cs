﻿using Abp.Domain.Entities;
using Abp.Runtime.Security;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VDI.Demo.PropertySystemDB.Notification
{
    [Table("MS_MappingDB")]
    public class MS_MappingDB : Entity
    {
        [Required]
        [StringLength(30)]
        public string InitialCatalog { get; set; }

        [Required]
        [StringLength(20)]
        public string DataSource { get; set; }

        [Required]
        [StringLength(20)]
        public string UserName { get; set; }

        [Required]
        public string Password   { get; set; }

        [NotMapped]
        public string SecurePassword
        {
            get
            {
                return SimpleStringCipher.Instance.Decrypt(this.Password);
            }
            set {
                this.Password = SimpleStringCipher.Instance.Encrypt(value);
            }
        }
    }
}
