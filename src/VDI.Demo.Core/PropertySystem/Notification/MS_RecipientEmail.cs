﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VDI.Demo.PropertySystemDB.Notification
{
    [Table("MS_RecipientEmail")]
    public class MS_RecipientEmail : AuditedEntity
    {
        [StringLength(8)]
        public string psCode { get; set; }
        
        [StringLength(100)]
        public string psName { get; set; }
        
        [StringLength(20)]
        public string bookCode { get; set; }
        
        [StringLength(100)]
        public string email { get; set; }
    }
}
