﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using VDI.Demo.PropertySystemDB.MasterPlan.Unit;

namespace VDI.Demo.PropertySystemDB.OnlineBooking.ProjectInfo
{
    [Table("MS_ProjectUnit")]
    public class MS_ProjectUnit : AuditedEntity
    {
        [ForeignKey("MS_Unit")]
        public int? unitID { get; set; }
        public virtual MS_Unit MS_Unit { get; set; }

        public bool isShow { get; set; }

        //[StringLength(100)]
        //public string excludeRenovID { get; set; }
    }
}
