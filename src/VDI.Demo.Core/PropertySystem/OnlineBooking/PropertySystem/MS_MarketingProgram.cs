﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using VDI.Demo.PropertySystemDB.MasterPlan.Project;

namespace VDI.Demo.PropertySystemDB.OnlineBooking.PropertySystem
{
    [Table("MS_MarketingProgram")]
    public class MS_MarketingProgram : AuditedEntity
    {
        public string MarketingProgram { get; set; }

        [ForeignKey("MS_Project")]
        public int? projectID { get; set; }
        public virtual MS_Project MS_Project { get; set; }

        public DateTime startDate { get; set; }
        public DateTime endDate { get; set; }
    }
}
