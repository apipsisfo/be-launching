﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using VDI.Demo.PropertySystemDB.OnlineBooking.ProjectInfo;

namespace VDI.Demo.PropertySystemDB.OnlineBooking.PropertySystem
{
    public class MS_SitePlan : AuditedEntity<long>
    {
        [ForeignKey("MS_ProjectInfo")]
        public int projectInfoID { get; set; }
        public virtual MS_ProjectInfo MS_ProjectInfo { get; set; }

        public string siteplanDir { get; set; }

        public string viewBox { get; set; }

        public ICollection<MS_SvgArea> MS_SvgArea { get; set; }
    }
}
