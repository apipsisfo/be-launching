﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.PropertySystemDB.OnlineBooking.PropertySystem
{
    public class SYS_OrderCounter : AuditedEntity
    {
        public int projectID { get; set; }

        public int orderNo { get; set; }

        public int entityID { get; set; }
    }
}
