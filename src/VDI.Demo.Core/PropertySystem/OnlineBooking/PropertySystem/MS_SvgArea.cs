﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VDI.Demo.PropertySystemDB.OnlineBooking.PropertySystem
{
    public class MS_SvgArea : AuditedEntity<long>
    {
        [ForeignKey("MS_SitePlan")]
        public long sitePlanID { get; set; }
        public virtual MS_SitePlan MS_SitePlan { get; set; }

        [StringLength(10)]
        public string svgID { get; set; }

        [StringLength(50)]
        public string svgTitle { get; set; }
    }
}
