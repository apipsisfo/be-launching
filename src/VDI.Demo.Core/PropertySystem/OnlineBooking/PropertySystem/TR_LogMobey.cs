﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VDI.Demo.PropertySystemDB.OnlineBooking.PropertySystem
{
    [Table("TR_LogMobey")]
    public class TR_LogMobey : AuditedEntity
    {
        public string orderCode { get; set; }

        public string description { get; set; }

        public string messageError { get; set; }

        public DateTime? transactionTime { get; set; }

        public string transactionReference { get; set; }

        public bool isVoid { get; set; }

        public DateTime? voidTime { get; set; }
    }
}
