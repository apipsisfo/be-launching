﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using VDI.Demo.PropertySystemDB.MasterPlan.Unit;

namespace VDI.Demo.PropertySystemDB.OnlineBooking.PropertySystem
{
    [Table("MS_UnitVirtualAccount")]
    public class MS_UnitVirtualAccount : AuditedEntity
    {
        [ForeignKey("MS_Unit")]
        public int unitID { get; set; }
        public virtual MS_Unit MS_Unit { get; set; }

        [Required]
        [StringLength(100)]
        public string VA_BankName { get; set; }

        [Required]
        [StringLength(100)]
        public string VA_BankAccNo { get; set; }
        
    }
}
