﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VDI.Demo.PropertySystemDB.OnlineBooking.PropertySystem
{
    [Table("MS_MappingMidtrans")]
    public class MS_MappingMidtrans : AuditedEntity
    {
        public int projectID { get; set; }

        [StringLength(50)]
        public string environment { get; set; }

        [StringLength(100)]
        public string baseURLlite { get; set; }

        [StringLength(100)]
        public string baseURL { get; set; }

        [StringLength(100)]
        public string serverKey { get; set; }

        [StringLength(100)]
        public string clientKey { get; set; }

        [StringLength(30)]
        public string midtransName { get; set; }

        [StringLength(5)]
        public string businessGroup { get; set; }
    }
}
