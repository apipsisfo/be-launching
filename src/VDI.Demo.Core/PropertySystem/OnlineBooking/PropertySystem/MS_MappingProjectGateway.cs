﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VDI.Demo.PropertySystemDB.OnlineBooking.PropertySystem
{
    [Table("MS_MappingProjectGateway")]
    public class MS_MappingProjectGateway : AuditedEntity
    {
        public int ProjectID { get; set; }

        public int PaymentTypeID { get; set; }

        [Required]
        public string Gateway { get; set; }
    }
}
