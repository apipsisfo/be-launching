﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VDI.Demo.PropertySystemDB.OnlineBooking.PropertySystem
{
    [Table("MS_MappingKPByTerm")]
    public class MS_MappingKPByTerm : AuditedEntity
    {
        public int termID { get; set; }

        public int clusterID { get; set; }

        public string Remarks1 { get; set; }

        public string Remarks2 { get; set; }

        public DateTime StartPeriodeBooking { get; set; }

        public DateTime EndPeriodeBooking { get; set; }
    }
}
