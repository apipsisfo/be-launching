﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using VDI.Demo.PropertySystemDB.MasterPlan.Project;

namespace VDI.Demo.PropertySystemDB.OnlineBooking.PropertySystem
{
    [Table("MS_BookingLocation")]
    public class MS_BookingLocation : AuditedEntity
    {
        public string bookingLocationName { get; set; }

        [ForeignKey("MS_Project")]
        public int? projectID { get; set; }
        public virtual MS_Project MS_Project { get; set; }

        public DateTime startDate { get; set; }
        public DateTime endDate { get; set; }
    }
}
