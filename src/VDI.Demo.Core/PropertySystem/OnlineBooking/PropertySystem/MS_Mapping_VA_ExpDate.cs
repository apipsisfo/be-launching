﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VDI.Demo.PropertySystemDB.OnlineBooking.PropertySystem
{
    [Table("MS_Mapping_VA_ExpDate")]
    public class MS_Mapping_VA_ExpDate : AuditedEntity
    {
        public int ProjectInfoID { get; set; }

        public int PaymentType { get; set; }

        public int EntityID { get; set; }

        public int ExpHour { get; set; }
    }
}
