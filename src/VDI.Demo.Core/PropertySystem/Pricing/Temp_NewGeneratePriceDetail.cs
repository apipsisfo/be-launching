﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using VDI.Demo.PropertySystemDB.MasterPlan.Unit;

namespace VDI.Demo.PropertySystemDB.Pricing
{
    [Table("Temp_NewGeneratePriceDetail")]
    public class Temp_NewGeneratePriceDetail : AuditedEntity
    {

        [ForeignKey("Temp_NewGeneratePriceHeader")]
        public int newGeneratePriceHeaderID { get; set; }
        public virtual Temp_NewGeneratePriceHeader Temp_NewGeneratePriceHeader { get; set; }


        [ForeignKey("MS_Unit")]
        public int unitID { get; set; }
        public virtual MS_Unit MS_Unit { get; set; }

        [ForeignKey("MS_Cluster")]
        public int clusterID { get; set; }
        public virtual MS_Cluster MS_Cluster { get; set; }
        
        [ForeignKey("MS_Term")]
        public int termID { get; set; }
        public virtual MS_Term MS_Term { get; set; }

        [Column(TypeName = "money")]
        public decimal cost { get; set; }
        public double margin { get; set; }

        [Column(TypeName = "money")]
        public decimal basePrice { get; set; }
        public double luas { get; set; }
        public double factoring { get; set; }

        [Column(TypeName = "money")]
        public decimal priceTanah { get; set; }
        public double pctTanah { get; set; }

        [Column(TypeName = "money")]
        public decimal priceBangunan { get; set; }
        public double pctBangunan { get; set; }
        
        [Column(TypeName = "money")]
        public decimal priceRenov { get; set; }
        public double pctRenov { get; set; }

        [Column(TypeName = "money")]
        public decimal totalPrice { get; set; }
        public double discount { get; set; }

        [Column(TypeName = "money")]
        public decimal netPrice { get; set; }

        [Column(TypeName = "money")]
        public decimal netNetPrice { get; set; }
        public double vat { get; set; }

        [Column(TypeName = "money")]
        public decimal sellingPrice { get; set; }
    }
}
