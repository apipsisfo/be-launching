﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using VDI.Demo.PropertySystemDB.MasterPlan.Unit;

namespace VDI.Demo.PropertySystemDB.Pricing
{
    [Table("Temp_DistrictClusterNewGeneratePrice")]
    public class Temp_DistrictClusterNewGeneratePrice : AuditedEntity
    {
        [ForeignKey("Temp_NewGeneratePriceHeader")]
        public int newGeneratePriceHeaderID { get; set; }
        public virtual Temp_NewGeneratePriceHeader Temp_NewGeneratePriceHeader { get; set; }

        [ForeignKey("MS_Cluster")]
        public int clusterID { get; set; }
        public virtual MS_Cluster MS_Cluster { get; set; }

        [Required]
        [StringLength(15)]
        public string DistrictCode { get; set; }
    }
}
