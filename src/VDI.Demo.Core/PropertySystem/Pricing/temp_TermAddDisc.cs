﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VDI.Demo.PropertySystemDB.Pricing
{
    [Table("temp_TermAddDisc")]
    public class temp_TermAddDisc : AuditedEntity
    {

        public byte addDiscNo { get; set; }

        public double addDiscPct { get; set; }

        public decimal addDiscAmt { get; set; }

        public bool isAmount { get; set; }

        public int entityID { get; set; }

        [ForeignKey("MS_Term")]
        public int termID { get; set; }
        public virtual MS_Term MS_Term { get; set; }

        [ForeignKey("MS_Discount")]
        public int discountID { get; set; }
        public virtual MS_Discount MS_Discount { get; set; }


        [StringLength(30)]
        public string GeneratePriceDiscountCode { get; set; }
    }
}
