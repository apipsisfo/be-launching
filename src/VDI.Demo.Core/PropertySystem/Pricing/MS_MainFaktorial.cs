﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using VDI.Demo.PropertySystemDB.MasterPlan.Project;

namespace VDI.Demo.PropertySystemDB.Pricing
{
    [Table("MS_MainFaktorial")]
    public class MS_MainFaktorial : AuditedEntity
    {
        [ForeignKey("MS_Project")]
        public int projectID { get; set; }
        public virtual MS_Project MS_Project { get; set; }
        [Required]
        [StringLength(20)]
        public string MainFaktorialCode { get; set; }
        [Required]
        [StringLength(100)]
        public string MainFaktorialName { get; set; }
        public string Remarks { get; set; }
        public bool isBasePrice { get; set; }
        public bool isParameter { get; set; }
    }
}
