﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using VDI.Demo.PropertySystemDB.MasterPlan.Project;

namespace VDI.Demo.PropertySystemDB.Pricing
{
    [Table("MS_MappingFaktorial")]
    public class MS_MappingFaktorial : AuditedEntity
    {
        [ForeignKey("MS_Project")]
        public int projectID { get; set; }
        public virtual MS_Project MS_Project { get; set; }

        [StringLength(100)]
        public string MappingFaktorialCode { get;set; }
        [StringLength(100)]
        public string MappingFaktorialName { get; set; }
        public string Remarks { get; set; }
    }
}
