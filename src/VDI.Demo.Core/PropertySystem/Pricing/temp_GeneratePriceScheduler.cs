﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using VDI.Demo.PropertySystemDB.MasterPlan.Unit;

namespace VDI.Demo.PropertySystemDB.Pricing
{
    [Table("temp_GeneratePriceScheduler")]
    public class temp_GeneratePriceScheduler : AuditedEntity
    {
        [ForeignKey("MS_Unit")]
        public int unitID { get; set; }
        public virtual MS_Unit MS_Unit { get; set; }

        [StringLength(20)]
        public string unitCode { get; set; }
        [StringLength(8)]
        public string unitNo { get; set; }

        [ForeignKey("MS_Term")]
        public int termID { get; set; }
        public virtual MS_Term MS_Term { get; set; }

        public decimal BasePricePerMeter { get; set; }
        public decimal LuasUnit { get; set; }
        public decimal percentageFaktorial { get; set; }
        public decimal percentageSwing { get; set; }
        public decimal TotalPricing { get; set; }
        public DateTime? SchedulerDate { get; set; }
        public bool StatusScheduler { get; set; }

        [StringLength(30)]
        public string GeneratePriceDiscountCode { get; set; }

        [ForeignKey("MS_Renovation")]
        public int renovID { get; set; }
        public virtual MS_Renovation MS_Renovation { get; set; }
    }
}
