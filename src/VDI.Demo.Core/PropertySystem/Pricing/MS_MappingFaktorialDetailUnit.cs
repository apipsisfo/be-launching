﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using VDI.Demo.PropertySystemDB.MasterPlan.Unit;

namespace VDI.Demo.PropertySystemDB.Pricing
{
    [Table("MS_MappingFaktorialDetailUnit")]
    public class MS_MappingFaktorialDetailUnit : AuditedEntity
    {
        [ForeignKey("MS_MappingFaktorialDetail")]
        public int MappingFaktorialDetailID { get; set; }
        public virtual MS_MappingFaktorialDetail MS_MappingFaktorialDetail { get; set; }

        [ForeignKey("MS_Unit")]
        public int unitID { get; set; }
        public virtual MS_Unit MS_Unit { get; set; }
    }
}
