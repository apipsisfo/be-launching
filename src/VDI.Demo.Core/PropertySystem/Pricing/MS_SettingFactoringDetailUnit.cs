﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using VDI.Demo.PropertySystemDB.MasterPlan.Unit;

namespace VDI.Demo.PropertySystemDB.Pricing
{
    [Table("MS_SettingFactoringDetailUnit")]
    public class MS_SettingFactoringDetailUnit : AuditedEntity
    {
        [ForeignKey("MS_SettingFactoringDetail")]
        public int SettingFactoringDetailID { get; set; }
        public virtual MS_SettingFactoringDetail MS_SettingFactoringDetail { get; set; }

        [ForeignKey("MS_Unit")]
        public int unitID { get; set; }
        public virtual MS_Unit MS_Unit { get; set; }
    }
}
