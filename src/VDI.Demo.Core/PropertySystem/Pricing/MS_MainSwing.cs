﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using VDI.Demo.PropertySystemDB.MasterPlan.Project;

namespace VDI.Demo.PropertySystemDB.Pricing
{
    [Table("MS_MainSwing")]
    public class MS_MainSwing : AuditedEntity
    {

        [ForeignKey("MS_Project")]
        public int projectID { get; set; }
        public virtual MS_Project MS_Project { get; set; }


        [ForeignKey("MS_TermMain")]
        public int termMainID { get; set; }
        public virtual MS_TermMain MS_TermMain { get; set; }

        [StringLength(50)]
        public string MainSwingCode { get; set; }
        [StringLength(100)]
        public string MainSwingName { get; set; }
    }
}
