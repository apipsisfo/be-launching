﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using VDI.Demo.PropertySystemDB.MasterPlan.Project;
using VDI.Demo.PropertySystemDB.MasterPlan.Unit;

namespace VDI.Demo.PropertySystemDB.Pricing
{
    [Table("MS_MappingFaktorialDetail")]
    public class MS_MappingFaktorialDetail : AuditedEntity
    {
        [ForeignKey("MS_MappingFaktorial")]
        public int MappingFaktorialID { get; set; }
        public virtual MS_MappingFaktorial MS_MappingFaktorial { get; set; }

        [ForeignKey("MS_Faktorial")]
        public int FaktorialID { get; set; }
        public virtual MS_Faktorial MS_Faktorial { get; set; }

        [StringLength(15)]
        public string DistrictCode { get; set; }

        [StringLength(25)]
        public string blok { get; set; }

        [StringLength(25)]
        public string clusterType { get; set; }

        [ForeignKey("MS_Cluster")]
        public int? clusterID { get; set; }
        public virtual MS_Cluster MS_Cluster { get; set; }


        [StringLength(25)]
        public string unitType { get; set; }

    }
}
