﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VDI.Demo.PropertySystemDB.Pricing
{
    [Table("MS_Faktorial")]
    public class MS_Faktorial : AuditedEntity
    {
        [ForeignKey("MS_MainFaktorial")]
        public int MainFaktorialID { get; set; }
        public virtual MS_MainFaktorial MS_MainFaktorial { get; set; }
        [StringLength(20)]
        public string FaktorialCode { get; set; }
        [StringLength(100)]
        public string FaktorialName { get; set; }

        [ForeignKey("MS_FaktorialParameter")]
        public int? FaktorialParameterID { get; set; }
        public virtual MS_FaktorialParameter MS_FaktorialParameter { get; set; }

        public string FaktorialValues { get; set; }
    }
}
