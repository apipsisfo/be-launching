﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using VDI.Demo.PropertySystemDB.MasterPlan.Project;

namespace VDI.Demo.PropertySystemDB.Pricing
{
    [Table("MS_SettingFactoring")]
    public class MS_SettingFactoring : AuditedEntity
    {
        [ForeignKey("MS_Project")]
        public int projectID { get; set; }
        public virtual MS_Project MS_Project { get; set; }

        [StringLength(100)]
        public string FactoringCode { get; set; }
        [StringLength(100)]
        public string FactoringName { get; set; }
        public string Remarks { get; set; }
    }
}
