﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using VDI.Demo.PropertySystemDB.MasterPlan.Unit;

namespace VDI.Demo.PropertySystemDB.Pricing
{
    [Table("temp_MainDisc")]
    public class temp_MainDisc : AuditedEntity
    {
        public decimal mainDisc { get; set; }

        [ForeignKey("MS_Cluster")]
        public int clusterID { get; set; }
        public virtual MS_Cluster MS_Cluster { get; set; }


        [ForeignKey("MS_Discount")]
        public int discountID { get; set; }
        public virtual MS_Discount MS_Discount { get; set; }


        [StringLength(30)]
        public string GeneratePriceDiscountCode { get; set; }
        public bool isAmount { get; set; }
    }
}
