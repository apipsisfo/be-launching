﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using VDI.Demo.PropertySystemDB.MasterPlan.Project;

namespace VDI.Demo.PropertySystemDB.Pricing
{
    [Table("temp_workflowGeneratePriceHeader")]
    public class temp_workflowGeneratePriceHeader : AuditedEntity
    {
        [ForeignKey("MS_Project")]
        public int projectID { get; set; }
        public virtual MS_Project MS_Project { get; set; }
        
        public int WorkflowRequestID { get; set; }
        [StringLength(30)]
        public string GeneratePriceDiscountCode { get; set; }
        public bool isScheduler { get; set; }
        public DateTime? schedulerDate { get; set; }
    }
}
