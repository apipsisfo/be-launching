﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VDI.Demo.PropertySystemDB.Pricing
{
    [Table("Temp_ApprovalLog")]
    public class Temp_ApprovalLog : AuditedEntity
    {
        [StringLength(100)]
        public string UserName { get; set; }

        [ForeignKey("Temp_StatusGeneratePrice")]
        public int statusGeneratePriceId { get; set; }
        public virtual Temp_StatusGeneratePrice Temp_StatusGeneratePrice { get; set; }

        [StringLength(400)]
        public string Remarks { get; set; }


        [ForeignKey("Temp_NewGeneratePriceHeader")]
        public int newGeneratePriceHeaderID { get; set; }
        public virtual Temp_NewGeneratePriceHeader Temp_NewGeneratePriceHeader { get; set; }
    }
}
