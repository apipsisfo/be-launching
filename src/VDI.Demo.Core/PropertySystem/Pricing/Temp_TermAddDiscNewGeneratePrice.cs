﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VDI.Demo.PropertySystemDB.Pricing
{
    [Table("Temp_TermAddDiscNewGeneratePrice")]
    public class Temp_TermAddDiscNewGeneratePrice : AuditedEntity
    {
        public byte addDiscNo { get; set; }

        public double addDiscPct { get; set; }

        public decimal addDiscAmt { get; set; }

        public bool isAmount { get; set; }

        public int entityID { get; set; }

        [ForeignKey("MS_Term")]
        public int termID { get; set; }
        public virtual MS_Term MS_Term { get; set; }

        [ForeignKey("MS_Discount")]
        public int discountID { get; set; }
        public virtual MS_Discount MS_Discount { get; set; }
        
        [ForeignKey("Temp_TermNewGeneratePrice")]
        public int termNewGeneratePriceID { get; set; }
        public virtual Temp_TermNewGeneratePrice Temp_TermNewGeneratePrice { get; set; }
    }
}
