﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VDI.Demo.PropertySystemDB.Pricing
{
    [Table("MS_FaktorialParameter")]
    public class MS_FaktorialParameter : AuditedEntity
    {
        [StringLength(100)]
        public string FaktorialParameterName { get; set; }
        public string FaktorialFormula { get; set; }
        public string Remarks { get; set; }
    }
}
