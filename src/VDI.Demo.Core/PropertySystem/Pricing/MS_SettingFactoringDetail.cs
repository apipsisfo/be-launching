﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using VDI.Demo.PropertySystemDB.MasterPlan.Unit;

namespace VDI.Demo.PropertySystemDB.Pricing
{
    [Table("MS_SettingFactoringDetail")]
    public class MS_SettingFactoringDetail : AuditedEntity
    {
        [ForeignKey("MS_SettingFactoring")]
        public int SettingFactoringID { get; set; }
        public virtual MS_SettingFactoring MS_SettingFactoring { get; set; }
        
        [StringLength(15)]
        public string DistrictCode { get; set; }

        [StringLength(25)]
        public string blok { get; set; }

        [StringLength(25)]
        public string clusterType { get; set; }

        [ForeignKey("MS_Cluster")]
        public int? clusterID { get; set; }
        public virtual MS_Cluster MS_Cluster { get; set; }

        [StringLength(25)]
        public string unitType { get; set; }

        public double Percentage { get; set; }

    }
}
