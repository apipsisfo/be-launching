﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using VDI.Demo.PropertySystemDB.MasterPlan.Project;
using VDI.Demo.PropertySystemDB.MasterPlan.Unit;

namespace VDI.Demo.PropertySystemDB.Pricing
{
    [Table("Temp_NewGeneratePriceHeader")]
    public class Temp_NewGeneratePriceHeader : AuditedEntity
    {
        //firstTab
        [ForeignKey("MS_Project")]
        public int projectID { get; set; }
        public virtual MS_Project MS_Project { get; set; }

        [Required]
        [StringLength(15)]
        public string DistrictCode { get; set; }
                
        [Column(TypeName = "money")]
        public decimal cost { get; set; }
        
        public double margin { get; set; }
        
        public DateTime startValidFrom { get; set; }
        
        [Required]
        [StringLength(150)]
        public string eventName { get; set; }

        [Required]
        [StringLength(100)]
        public string towerType { get; set; }

        [Required]
        [StringLength(100)]
        public string unitType { get; set; }
        //end FirstTab

        //secondTab
        [ForeignKey("MS_SettingFactoring")]
        public int? factoringID { get; set; }
        public virtual MS_SettingFactoring MS_SettingFactoring { get; set; }

        public double? discount { get; set; }
        public double? vat { get; set; }
               
        [ForeignKey("MS_TermMain")]
        public int? termMainID { get; set; }
        public virtual MS_TermMain MS_TermMain { get; set; }


        [ForeignKey("MS_Renovation")]
        public int? renovID { get; set; }
        public virtual MS_Renovation MS_Renovation { get; set; }

        //end secondTab

        //process Data
        public int? WorkflowRequestID { get; set; }
        public bool? statusScheduler { get; set; }
        public DateTime? reqStartValidFrom { get; set; }


        [ForeignKey("Temp_StatusGeneratePrice")]
        public int statusGeneratePriceId { get; set; }
        public virtual Temp_StatusGeneratePrice Temp_StatusGeneratePrice { get; set; }
        //end process Data
    }
}
