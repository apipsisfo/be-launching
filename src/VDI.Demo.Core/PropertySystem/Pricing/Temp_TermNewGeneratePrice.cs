﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VDI.Demo.PropertySystemDB.Pricing
{
    [Table("Temp_TermNewGeneratePrice")]
    public class Temp_TermNewGeneratePrice : AuditedEntity
    {
        [ForeignKey("Temp_NewGeneratePriceHeader")]
        public int newGeneratePriceHeaderID { get; set; }
        public virtual Temp_NewGeneratePriceHeader Temp_NewGeneratePriceHeader { get; set; }
        
        [ForeignKey("MS_Term")]
        public int termID { get; set; }
        public virtual MS_Term MS_Term { get; set; }

        public double termPercentage { get; set; }
    }
}
