﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VDI.Demo.PropertySystemDB.Pricing
{
    [Table("MS_Swing")]
    public class MS_Swing : AuditedEntity
    {
        [ForeignKey("MS_MainSwing")]
        public int mainSwingID { get; set; }
        public virtual MS_MainSwing MS_MainSwing { get; set; }

        public int FromTermID { get; set; }
        public int ToTermID { get; set; }
        public decimal swingPercentage { get; set; }
    }
}
