﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VDI.Demo.PropertySystemDB.Pricing
{
    [Table("Temp_StatusGeneratePrice")]
    public class Temp_StatusGeneratePrice : AuditedEntity
    {
        [StringLength(100)]
        public string StatusGeneratePrice { get; set; }
        [StringLength(100)]
        public string StatusLog { get; set; }
        public int? identifierNumber { get; set; }
        public int? actualFlowNumber { get; set; }
    }
}
