﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VDI.Demo.PropertySystemDB.Diagramatic
{
    [Table("MS_UnitCode")]
    public class MS_UnitCode2 : AuditedEntity
    {
        public int entityID { get; set; }

        //unique
        [StringLength(20)]
        public string unitCode { get; set; }

        [Required]
        [StringLength(50)]
        public string unitName { get; set; }
        
        public int projectID { get; set; }

        public string floor { get; set; }

        public ICollection<MS_Unit2> MS_Unit { get; set; }
    }
}
