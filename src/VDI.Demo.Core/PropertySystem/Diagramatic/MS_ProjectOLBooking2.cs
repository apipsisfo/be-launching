﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VDI.Demo.PropertySystemDB.Diagramatic
{
    [Table("MS_ProjectOLBooking")]
    public class MS_ProjectOLBooking2 : AuditedEntity
    {
        
        public int? projectID { get; set; }
        
        public int clusterID { get; set; }

        [StringLength(100)]
        public string projectName { get; set; }

        [StringLength(100)]
        public string clusterName { get; set; }

        [StringLength(1000)]
        public string projectDesc { get; set; }

        [StringLength(1000)]
        public string imgLogo { get; set; }

        [StringLength(200)]
        public string handOverPeriod { get; set; }

        [StringLength(200)]
        public string graceOverPeriod { get; set; }

        public bool? isActive { get; set; }

        public DateTime? activeFrom { get; set; }

        public DateTime? activeTo { get; set; }

        public bool? isRequiredPP { get; set; }

        [StringLength(50)]
        public string diagramaticType { get; set; }
        
        public int? projectInfoID { get; set; }
    }
}
