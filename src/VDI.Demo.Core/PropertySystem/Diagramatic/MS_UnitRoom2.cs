﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VDI.Demo.PropertySystemDB.Diagramatic
{
    [Table("MS_UnitRoom")]
    public class MS_UnitRoom2 : AuditedEntity
    {
        public int unitItemID { get; set; }

        [StringLength(50)]
        public string bedroom { get; set; }

        [StringLength(50)]
        public string bathroom { get; set; }
    }
}
