﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VDI.Demo.PropertySystemDB.Diagramatic
{
    [Table("MS_Unit")]
    public class MS_Unit2 : AuditedEntity
    {

        //unique
        [Required]
        [StringLength(8)]
        public string unitNo { get; set; }

        [Required]
        [StringLength(8)]
        public string CombinedUnitNo { get; set; }

        [Required]
        [StringLength(1)]
        public string unitCertCode { get; set; }

        [Required]
        [StringLength(100)]
        public string remarks { get; set; }

        [Required]
        [StringLength(8)]
        public string prevUnitNo { get; set; }

        public int entityID { get; set; }
        
        public int unitCodeID { get; set; }
        
        public int areaID { get; set; }
        
        public int projectID { get; set; }
        
        public int categoryID { get; set; }
        
        public int clusterID { get; set; }
        
        public int productID { get; set; }
        
        public int detailID { get; set; }
        
        public int zoningID { get; set; }
        
        public int facingID { get; set; }
        
        public int unitStatusID { get; set; }
        
        public int rentalStatusID { get; set; }
        
        public int? termMainID { get; set; }

        public int? TokenNo { get; set; }
    }
}
