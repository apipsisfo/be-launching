﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VDI.Demo.PropertySystemDB.Diagramatic
{
    [Table("MS_ProjectInfo")]
    public class MS_ProjectInfo2 : AuditedEntity
    {
        public int? projectID { get; set; }

        [Column("productName")]
        public string displayName { get; set; }
        
        public string productCode { get; set; }

        //[StringLength(100)]
        public string projectDesc { get; set; }
        
        public int keyFeaturesCollectionID { get; set; }

        [StringLength(100)]
        public string projectDeveloper { get; set; }

        [StringLength(100)]
        public string projectWebsite { get; set; }

        [StringLength(500)]
        public string projectMarketingOffice { get; set; }

        [StringLength(50)]
        public string projectMarketingPhone { get; set; }

        [StringLength(200)]
        public string projectImageLogo { get; set; }

        [StringLength(100)]
        public string sitePlansImageUrl { get; set; }

        [StringLength(1000)]
        public string sitePlansLegend { get; set; }

        public bool projectStatus { get; set; }

        [StringLength(500)]
        public string displaySetting { get; set; }

        public bool isOBActive { get; set; }

        public bool isPPOLActive { get; set; }
    }
}
