﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VDI.Demo.PropertySystemDB.Diagramatic
{
    [Table("LK_Item")]
    public class LK_Item2 : AuditedEntity
    {
        //unique
        [Required]
        [StringLength(2)]
        public string itemCode { get; set; }

        [Required]
        [StringLength(40)]
        public string itemName { get; set; }

        [Required]
        [StringLength(15)]
        public string shortName { get; set; }

        [Required]
        public int sortNo { get; set; }

        [Required]
        public bool isOption { get; set; }

        [Required]
        public int optionSort { get; set; }
    }
}
