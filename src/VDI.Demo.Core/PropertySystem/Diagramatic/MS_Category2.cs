﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VDI.Demo.PropertySystemDB.Diagramatic
{
    [Table("MS_Category")]
    public class MS_Category2 : AuditedEntity
    {
        //unique
        [Required]
        [StringLength(5)]
        public string categoryCode { get; set; }

        [Required]
        [StringLength(30)]
        public string categoryName { get; set; }

        [Required]
        [StringLength(30)]
        public string projectField { get; set; }

        [Required]
        [StringLength(30)]
        public string areaField { get; set; }

        [Required]
        [StringLength(30)]
        public string categoryField { get; set; }

        [Required]
        [StringLength(30)]
        public string clusterField { get; set; }

        [Required]
        [StringLength(30)]
        public string productField { get; set; }

        [Required]
        [StringLength(30)]
        public string detailField { get; set; }

        [Required]
        [StringLength(30)]
        public string zoningField { get; set; }

        [Required]
        [StringLength(30)]
        public string facingField { get; set; }

        [Required]
        [StringLength(30)]
        public string roadField { get; set; }

        [Required]
        [StringLength(30)]
        public string kavNoField { get; set; }

        [StringLength(50)]
        public string kadasterType { get; set; }

        [StringLength(50)]
        public string diagramaticType { get; set; }

        public int entityID { get; set; }
    }
}
