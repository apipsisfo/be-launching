﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VDI.Demo.PropertySystemDB.Diagramatic
{
    [Table("MS_Zoning")]
    public class MS_Zoning2 : AuditedEntity
    {
        public int entityID { get; set; }

        //unique
        [Required]
        [StringLength(8)]
        public string zoningCode { get; set; }

        [Required]
        [StringLength(50)]
        public string zoningName { get; set; }
    }
}
