﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VDI.Demo.PropertySystemDB.Diagramatic
{
    [Table("MS_ProjectUnit")]
    public class MS_ProjectUnit2 : AuditedEntity
    {
        public int? unitID { get; set; }

        public bool isShow { get; set; }

        //[StringLength(100)]
        //public string excludeRenovID { get; set; }
    }
}
