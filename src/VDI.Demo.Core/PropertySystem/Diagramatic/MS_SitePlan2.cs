﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.PropertySystemDB.Diagramatic
{
    public class MS_SitePlan2 : AuditedEntity<long>
    {
        public int projectInfoID { get; set; }

        public string siteplanDir { get; set; }

        public string viewBox { get; set; }
    }
}
