﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VDI.Demo.PropertySystemDB.Diagramatic
{
    [Table("MS_UnitItem")]
    public class MS_UnitItem2 : AuditedEntity
    {
        public int entityID { get; set; }
        
        public int unitID { get; set; }
        
        public int itemID { get; set; }

        [Required]
        [StringLength(5)]
        public string coCode { get; set; }

        [Required]
        [Column(TypeName = "money")]
        public decimal amount { get; set; }

        [Required]
        public double pctDisc { get; set; }

        [Required]
        public double pctTax { get; set; }

        [Required]
        public double area { get; set; }

        [Required]
        [StringLength(50)]
        public string dimension { get; set; }
    }
}
