﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace VDI.Demo.PropertySystemDB.Diagramatic
{
    public class MS_SvgArea2 : AuditedEntity<long>
    {
        public long sitePlanID { get; set; }

        [StringLength(10)]
        public string svgID { get; set; }

        [StringLength(50)]
        public string svgTitle { get; set; }
    }
}
