﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using VDI.Demo.PropertySystemDB.OnlineBooking.ProjectInfo;

namespace VDI.Demo.PropertySystemDB.PPOnline
{
    [Table("MS_PPNonRefund")]
    public class MS_PPNonRefund : AuditedEntity
    {
        [ForeignKey("MS_ProjectInfo")]
        public int projectInfoID { get; set; }
        public virtual MS_ProjectInfo LK_PPStatus { get; set; }

        public DateTime startDate { get; set; }

        public DateTime endDate { get; set; }

        [StringLength(50)]
        public string type { get; set; }

        public string remark { get; set; }
    }
}
