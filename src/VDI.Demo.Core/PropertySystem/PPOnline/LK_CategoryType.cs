﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using VDI.Demo.PropertySystemDB.PPOnline;

namespace VDI.Demo.PropertySystemDB.PPOnline
{
    public class LK_CategoryType : AuditedEntity
    {
        public int categoryType { get; set; }

        [Required]
        [StringLength(50)]
        public string categoryTypeName { get; set; }

        public ICollection<MS_ProjectPPOnline> MS_ProjectPPOnline { get; set; }
    }
}
