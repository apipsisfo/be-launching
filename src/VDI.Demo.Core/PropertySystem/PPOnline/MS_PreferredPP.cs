﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using VDI.Demo.PropertySystemDB.MasterPlan.Unit;
using VDI.Demo.PropertySystemDB.OnlineBooking.ProjectInfo;

namespace VDI.Demo.PropertySystemDB.PPOnline
{
    [Table("MS_PreferredPP")]
    public class MS_PreferredPP : AuditedEntity
    {
        [ForeignKey("MS_ProjectInfo")]
        public int projectInfoID { get; set; }
        public virtual MS_ProjectInfo MS_ProjectInfo { get; set; }

        [ForeignKey("MS_Detail")]
        public int detailID { get; set; }
        public virtual MS_Detail MS_Detail { get; set; }
    }
}