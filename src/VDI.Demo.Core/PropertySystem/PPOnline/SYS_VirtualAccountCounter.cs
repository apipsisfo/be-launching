﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using VDI.Demo.PropertySystemDB.MasterPlan.Project;

namespace VDI.Demo.PropertySystemDB.PPOnline
{
    [Table("SYS_VirtualAccountCounter")]
    public class SYS_VirtualAccountCounter : AuditedEntity
    {
        [ForeignKey("MS_Project")]
        public int projectID { get; set; }
        public virtual MS_Project MS_Project { get; set; }

        [Required]
        [StringLength(20)]
        public string virtualAccProject { get; set; }

        public int virtualAccNo { get; set; }
    }
}
