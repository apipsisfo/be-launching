﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using VDI.Demo.PropertySystemDB.MasterPlan.Project;

namespace VDI.Demo.PropertySystemDB.PPOnline
{
    [Table("MS_PPFlagStatus")]
    public class MS_PPFlagStatus : AuditedEntity
    {
        [ForeignKey("MS_Project")]
        public int projectID { get; set; }
        public virtual MS_Project MS_Project { get; set; }

        public int defaultChecklist { get; set; } //0 = tidak ada default, 1 = default refundable, 2 = default transferable, 3 = default fastlane

        public bool isTransferable { get; set; }

        public bool isRefundable { get; set; }

        public bool isFastLane { get; set; }
        public string FastLaneName { get; set; }
        public bool isNon { get; set; }
        public int mappingFastLane { get; set; }
    }
}
