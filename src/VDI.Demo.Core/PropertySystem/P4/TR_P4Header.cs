﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using VDI.Demo.PropertySystemDB.MasterPlan.Project;

namespace VDI.Demo.PropertySystemDB.P4
{
    [Table("TR_P4Header")]
    public class TR_P4Header :AuditedEntity
    {
        [ForeignKey("MS_Project")]
        public int projectID { get; set; }
        public virtual MS_Project MS_Project { get; set; }

        //[ForeignKey("MS_P4Status")]
        //public int statusID { get; set; }
        //public virtual MS_P4Status MS_P4Status { get; set; }

        public int stateID { get; set; }

        [StringLength(100)]
        public string approvalPSAS { get; set; }

        [StringLength(100)]
        public string approvalFinance { get; set; }

        public ICollection<TR_P4APPDetail> TR_P4APPDetail { get; set; }

    }
}
