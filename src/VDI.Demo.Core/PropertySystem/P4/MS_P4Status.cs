﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VDI.Demo.PropertySystemDB.P4
{
    [Table("MS_P4Status")]
    public class MS_P4Status : AuditedEntity
    {
        [Required]
        [StringLength(5)]
        public string statusCode { get; set; }

        [Required]
        public string statusName { get; set; }

        //public ICollection<TR_P4Header> TR_P4Header { get; set; }

    }
}
