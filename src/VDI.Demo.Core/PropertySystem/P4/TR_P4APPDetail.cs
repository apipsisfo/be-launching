﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VDI.Demo.PropertySystemDB.P4
{
    [Table("TR_P4APPDetail")]
    public class TR_P4APPDetail :AuditedEntity
    {
        [ForeignKey("TR_P4Header")]
        public int P4HeaderID { get; set; }
        public virtual TR_P4Header TR_P4Header { get; set; }

        [Required]
        public string bookCode { get; set; }

        [Required]
        public string psCode { get; set; }

        [Required]
        public string psName { get; set; }

        [Required]
        public string unitCode { get; set; }

        [Required]
        public string unitNo { get; set; }

        [Column(TypeName = "money")]
        public decimal refundAmt { get; set; }

        public bool isTransfer { get; set; }

        public int? reasonID { get; set; }

        [Column(TypeName = "money")]
        public decimal? lostAmount { get; set; }

        public string remarks { get; set; }
    }
}
