﻿using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VDI.Demo.PropertySystemDB.SMSCampaign
{
    [Table("Ms_ProjectSenderID")]
    public class Ms_ProjectSenderID : Entity<string>
    {
        [NotMapped]
        public override string Id
        {
            get
            {
                return SenderID +
                  "-" + ProjectCode;
            }
            set { /* nothing */ }
        }

        [Key]
        [Column(Order = 0)]
        [StringLength(25)]
        public string SenderID { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(5)]
        public string ProjectCode { get; set; }

        [Required]
        [StringLength(40)]
        public string CreatedBy { get; set; }

        public DateTime CreatedOn { get; set; }

        [StringLength(40)]
        public string ModifiedBy { get; set; }

        public DateTime? ModifiedOn { get; set; }
    }
}
