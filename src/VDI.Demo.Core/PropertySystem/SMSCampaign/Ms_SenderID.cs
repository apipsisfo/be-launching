﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VDI.Demo.PropertySystemDB.SMSCampaign
{
    [Table("Ms_ProjectSenderID")]
    public class Ms_SenderID
    {
        [Key]
        [StringLength(25)]
        public string SenderID { get; set; }

        [Required]
        [StringLength(250)]
        public string SenderName { get; set; }

        public int? BillingSiteID { get; set; }

        [Required]
        [StringLength(40)]
        public string CreatedBy { get; set; }

        public DateTime CreatedOn { get; set; }

        [StringLength(40)]
        public string ModifiedBy { get; set; }

        public DateTime? ModifiedOn { get; set; }
    }
}
