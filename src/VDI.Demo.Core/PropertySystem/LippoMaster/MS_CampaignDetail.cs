﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VDI.Demo.PropertySystemDB.LippoMaster
{
    public class MS_CampaignDetail : AuditedEntity
    {
        [ForeignKey("MS_CampaignHeader")]
        public int campaignHeaderID { get; set; }
        public virtual MS_CampaignHeader MS_CampaignHeader { get; set; }

        [ForeignKey("MS_Reward")]
        public int rewardID { get; set; }
        public virtual MS_Reward MS_Reward { get; set; }

        [Column(TypeName = "money")]
        public decimal idrValue { get; set; }

        public double discount { get; set; }

        [Column(TypeName = "money")]
        public decimal costToMSU { get; set; }

        public double paymentThreshold { get; set; }

        [Required]
        [StringLength(20)]
        public string beforeAfterHO { get; set; }
    }
}
