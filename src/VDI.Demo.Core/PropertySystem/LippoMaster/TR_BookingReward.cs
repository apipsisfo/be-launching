﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using VDI.Demo.PropertySystemDB.OnlineBooking.PropertySystem;

namespace VDI.Demo.PropertySystemDB.LippoMaster
{
    [Table("TR_BookingReward")]
    public class TR_BookingReward : AuditedEntity
    {
        [ForeignKey("MS_Reward")]
        public int? rewardID { get; set; }
        public virtual MS_Reward MS_Reward { get; set; }
        
        [ForeignKey("TR_UnitOrderHeader")]
        public int unitOrderHeaderID { get; set; }
        public virtual TR_UnitOrderHeader TR_UnitOrderHeader { get; set; }

        [ForeignKey("MS_Program")]
        public int? programID { get; set; }
        public virtual MS_Program MS_Program { get; set; }

        [Required]
        public string status { get; set; }

        public string voucherCode { get; set; }

        public int? isBookingUnit { get; set; } // 1 = dari booking unit sales web, 2 = dari app voucher

        //20200506-Penambahan field-Trisno(Analis)-Arini(Dev)
        public DateTime? newRewardDate { get; set; }
        public DateTime? inputVoucherDate { get; set; }
        public DateTime? redeemDate { get; set; }
        public long? picNewReward { get; set; }
        public long? picInputVoucher { get; set; }
        public long? picRedeem { get; set; }

        [ForeignKey("MS_CampaignHeader")]
        public int? campaignHeaderID { get; set; }
        public virtual MS_CampaignHeader MS_CampaignHeader { get; set; }
    }
}
