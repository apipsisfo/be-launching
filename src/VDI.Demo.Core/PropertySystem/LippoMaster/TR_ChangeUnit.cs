﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VDI.Demo.PropertySystemDB.LippoMaster
{
    [Table("TR_ChangeUnit")]
    public class TR_ChangeUnit : AuditedEntity
    {
        [Required]
        [StringLength(8)]
        public string psCode { get; set; }

        [Required]
        [StringLength(5)]
        public string projectCode { get; set; }

        [Required]
        [StringLength(20)]
        public string bookCode { get; set; }
        public DateTime bookDate { get; set; }
        public DateTime changeDate { get; set; }

        [Required]
        [StringLength(20)]
        public string unitCodeLama { get; set; }

        [Required]
        [StringLength(8)]
        public string unitNoLama { get; set; }

        [Required]
        [StringLength(20)]
        public string unitCodeBaru { get; set; }

        [Required]
        [StringLength(8)]
        public string unitNoBaru { get; set; }

        [Required]
        [StringLength(20)]
        public string orderCodeLama { get; set; }

        [StringLength(20)]
        public string orderCodeBaru { get; set; }

        [Required]
        [StringLength(20)]
        public string dealCloser { get; set; }

        public int sumberDanaID { get; set; }

        [Column(TypeName = "money")]
        public decimal BiayaAdmin { get; set; }

        [Column(TypeName = "money")]
        public decimal totalSebelumPotongan { get; set; }

        [Column(TypeName = "money")]
        public decimal totalSetelahPotongan { get; set; }

        public int termOfPayment { get; set; }

        public double? diskon { get; set; }

        [StringLength(100)]
        public string status { get; set; }
        
        [StringLength(12)]
        public string memberCode { get; set; }

        [StringLength(100)]
        public string memberName { get; set; }

        [StringLength(3)]
        public string scmCode { get; set; }

        public virtual ICollection<TR_ChangeUnitDetail> TR_ChangeUnitDetail { get; set; }
        public virtual ICollection<TR_ChangeUnitDetailDoc> TR_ChangeUnitDetailDoc { get; set; }
    }
}
