﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VDI.Demo.PropertySystemDB.LippoMaster
{
    [Table("MS_FPStatus")]
    public class MS_FPStatus : AuditedEntity
    {
        [Required]
        [StringLength(1)]
        public string statusCode { get; set; }

        [Required]
        [StringLength(20)]
        public string statusDesc { get; set; }
    }
}
