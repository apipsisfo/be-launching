﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VDI.Demo.PropertySystemDB.LippoMaster
{
    [Table("LOG_TR_BookingHeader")]
    public class LOG_TR_BookingHeader
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Id { get; set; }

        [Column(Order = 1)]
        [StringLength(3)]
        public string BFPayTypeCode { get; set; }

        [Column(Order = 2, TypeName = "datetime2")]
        public DateTime CreationTime { get; set; }

        public long? CreatorUserId { get; set; }

        [Column(Order = 3)]
        [StringLength(1)]
        public string DPCalcType { get; set; }

        [Column(Order = 4)]
        [StringLength(5)]
        public string KPRBankCode { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? LastModificationTime { get; set; }

        public long? LastModifierUserId { get; set; }

        [Column(Order = 5)]
        [StringLength(20)]
        public string NUP { get; set; }

        [Column(Order = 6)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public short PPJBDue { get; set; }

        [Column(Order = 7)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int SADStatusID { get; set; }

        public int? MS_TransFromId { get; set; }

        [Column(Order = 8)]
        [StringLength(50)]
        public string bankName { get; set; }

        [Column(Order = 9)]
        [StringLength(30)]
        public string bankNo { get; set; }

        [StringLength(50)]
        public string bankRekeningPemilik { get; set; }

        [Column(Order = 10)]
        [StringLength(20)]
        public string bookCode { get; set; }

        [Column(Order = 11, TypeName = "datetime2")]
        public DateTime bookDate { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? cancelDate { get; set; }

        [Column(Order = 12)]
        [StringLength(1)]
        public string discBFCalcType { get; set; }

        [Column(Order = 13)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int entityID { get; set; }

        [Column(Order = 14)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int eventID { get; set; }

        public int? facadeID { get; set; }

        [Column(Order = 15)]
        public bool isPenaltyStop { get; set; }

        public bool? isSK { get; set; }

        [Column(Order = 16)]
        public bool isSMS { get; set; }

        [Column(Order = 17)]
        [StringLength(12)]
        public string memberCode { get; set; }

        [Column(Order = 18)]
        [StringLength(100)]
        public string memberName { get; set; }

        [Column(Order = 19, TypeName = "money")]
        public decimal netPriceComm { get; set; }

        [StringLength(50)]
        public string nomorRekeningPemilik { get; set; }

        [Column(Order = 20)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int promotionID { get; set; }

        [Column(Order = 21)]
        [StringLength(8)]
        public string psCode { get; set; }

        [Column(Order = 22)]
        [StringLength(1500)]
        public string remarks { get; set; }

        [Column(Order = 23)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int shopBusinessID { get; set; }

        [Column(Order = 24)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int termID { get; set; }

        [Column(Order = 25)]
        [StringLength(200)]
        public string termRemarks { get; set; }

        [Column(Order = 26)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int transID { get; set; }

        [Column(Order = 27)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int unitID { get; set; }

        public int? sumberDanaID { get; set; }

        public int? tujuanTransaksiID { get; set; }

        [Column(Order = 28)]
        [StringLength(3)]
        public string scmCode { get; set; }

        [StringLength(10)]
        public string execMode { get; set; }

        [StringLength(50)]
        public string execUN { get; set; }

        public DateTime? execTime { get; set; }
    }
}
