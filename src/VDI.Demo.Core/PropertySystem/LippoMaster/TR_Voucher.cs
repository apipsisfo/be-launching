﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VDI.Demo.PropertySystemDB.LippoMaster
{
    public class TR_Voucher
    {
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int projectID { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(6)]
        public string voucherTypeCode { get; set; }

        [Key]
        [Column(Order = 2)]
        [StringLength(50)]
        public string voucherCode { get; set; }

        [Required]
        [StringLength(100)]
        public string receiptCode { get; set; }

        public decimal receiptAmt { get; set; }

        public DateTime receiptDate { get; set; }

        public DateTime? expirationDate { get; set; }

        public DateTime inputTime { get; set; }

        [Required]
        public string inputUN { get; set; }

        public decimal voucherAmt { get; set; }

        public int rewardID { get; set; }
        public string attachment { get; set; }
    }
}
