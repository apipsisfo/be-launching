﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VDI.Demo.PropertySystemDB.LippoMaster
{
    [Table("MS_MappingPayment")]
    public class MS_MappingPayment : AuditedEntity
    {
        public int projectID { get; set; }
        
        public bool PPnInHover { get; set; }

        public bool PPnInPreview { get; set; }

        public bool PPnInSummary { get; set; }

        public bool PPnInCart { get; set; }
    }
}
