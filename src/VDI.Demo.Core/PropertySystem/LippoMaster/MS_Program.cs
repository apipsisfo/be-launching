﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace VDI.Demo.PropertySystemDB.LippoMaster
{
    public class MS_Program : AuditedEntity
    {
        [Required]
        public string programName { get; set; }

        public ICollection<MS_CampaignHeader> MS_CampaignHeader { get; set; }
    }
}
