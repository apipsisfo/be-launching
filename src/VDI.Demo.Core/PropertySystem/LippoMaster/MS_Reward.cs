﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using VDI.Demo.PropertySystemDB.MasterPlan.Project;

namespace VDI.Demo.PropertySystemDB.LippoMaster
{
    [Table("MS_Reward")]
    public class MS_Reward : AuditedEntity
    {
        [Required]
        public string rewardName { get; set; }

        [ForeignKey("MS_Project")]
        public int? projectID { get; set; }
        public virtual MS_Project MS_Project { get; set; }

        public bool? isBookingFee { get; set; }

        public bool? isBilling { get; set; }

        public bool? isParking { get; set; }

        public bool? isActive { get; set; }

        public ICollection<TR_BookingReward> TR_BookingReward { get; set; }

        public ICollection<MS_CampaignDetail> MS_CampaignDetail { get; set; }
    }
}
