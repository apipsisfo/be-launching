﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using VDI.Demo.PropertySystemDB.MasterPlan.Project;

namespace VDI.Demo.PropertySystemDB.LippoMaster
{
    [Table("MS_MappingMap")]
    public class MS_MappingMap : AuditedEntity
    {
        [Required]
        [StringLength(8)]
        public string unitCode { get; set; }

        [Required]
        [StringLength(5)]
        public string tower { get; set; }

        [Required]
        [StringLength(5)]
        public string position { get; set; }
        
        [ForeignKey("MS_District")]
        public int districtID { get; set; }
        public virtual MS_District MS_District { get; set; }
        
        [ForeignKey("MS_Project")]
        public int projectID { get; set; }
        public virtual MS_Project MS_Project { get; set; }
    }
}
