﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VDI.Demo.PropertySystemDB.LippoMaster
{
    [Table("MS_District")]
    public class MS_District : AuditedEntity
    {
        [Required]
        [StringLength(20)]
        public string districtName { get; set; }

        public ICollection<MS_MappingMap> MS_MappingMap { get; set; }
    }
}
