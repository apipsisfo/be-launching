﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace VDI.Demo.PropertySystemDB.LippoMaster
{
    [Table("TR_SwitchUnit")]
    public class TR_SwitchUnit : AuditedEntity
    {
        [Required]
        [StringLength(5)]
        public string projectCode { get; set; }

        [Required]
        [StringLength(20)]
        public string bookCode { get; set; }

        public DateTime bookDate { get; set; }

        public DateTime switchDate { get; set; }

        [Required]
        [StringLength(20)]
        public string unitCodeLama { get; set; }

        [Required]
        [StringLength(8)]
        public string unitNoLama { get; set; }

        [Required]
        [StringLength(20)]
        public string unitCodeBaru { get; set; }

        [Required]
        [StringLength(8)]
        public string unitNoBaru { get; set; }

        [Required]
        [StringLength(20)]
        public string orderCodeLama { get; set; }

        [Required]
        [StringLength(20)]
        public string orderCodeBaru { get; set; }
    }
}
