﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VDI.Demo.PropertySystemDB.LippoMaster
{
    [Table("MS_ChangeUnitParameter")]
    public class MS_ChangeUnitParameter : AuditedEntity
    {
        [StringLength(100)]
        public string Description { get; set; }
        public virtual ICollection<TR_ChangeUnitDetail> TR_ChangeUnitDetail { get; set; }
    }
}
