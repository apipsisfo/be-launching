﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using VDI.Demo.PropertySystemDB.MasterPlan.Unit;

namespace VDI.Demo.PropertySystemDB.LippoMaster
{
    public class TR_SPB : AuditedEntity
    {
        [ForeignKey("MS_Unit")]
        public int unitID { get; set; }
        public virtual MS_Unit MS_Unit { get; set; }

        [ForeignKey("MS_Renovation")]
        public int RenovID { get; set; }
        public virtual MS_Renovation MS_Renovation { get; set; }
    }
}
