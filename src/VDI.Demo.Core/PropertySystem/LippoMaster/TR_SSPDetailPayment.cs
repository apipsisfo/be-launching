﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VDI.Demo.PropertySystemDB.LippoMaster
{
    [Table("TR_SSPDetailPayment")]
    public class TR_SSPDetailPayment : AuditedEntity
    {
        [Required]
        [StringLength(50)]
        public string SSPID { get; set; }

        [ForeignKey("TR_SSPMain")]
        public int sspMainID { get; set; }
        public virtual TR_SSPMain TR_SSPMain { get; set; }

        [ForeignKey("TR_BookingHeader")]
        public int bookingHeaderID { get; set; }
        public virtual TR_BookingHeader TR_BookingHeader { get; set; }

        [Required]
        [StringLength(10)]
        public string generateType { get; set; }

        [Required]
        [StringLength(30)]
        public string bookCode { get; set; }

        [Required]
        [StringLength(10)]
        public string psCode { get; set; }

        public DateTime bookDate { get; set; }

        public DateTime? cancelDate { get; set; }

        public float? areaLand { get; set; }

        public float? areaBuild { get; set; }

        [Column(TypeName = "money")]
        public decimal netNetPrice { get; set; }

        [Required]
        [StringLength(5)]
        public string payType { get; set; }

        [Column(TypeName = "money")]
        public decimal netAmtPayedPeriod { get; set; }

        [Column(TypeName = "money")]
        public decimal vatAmtPayedPeriod { get; set; }

        [Column(TypeName = "money")]
        public decimal totalNetAmtPayed { get; set; }

        public float ratePPh { get; set; }

        [Column(TypeName = "money")]
        public decimal PPhPaid { get; set; }

        [Column(TypeName = "money")]
        public decimal cadanganPPh { get; set; }

        [Column("modifUN")]
        public override long? LastModifierUserId { get; set; }

        [Column("inputTime")]
        public override DateTime CreationTime { get; set; }

        [Column("inputUN")]
        public override long? CreatorUserId { get; set; }
    }
}
