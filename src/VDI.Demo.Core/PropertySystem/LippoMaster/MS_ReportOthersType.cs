﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VDI.Demo.PropertySystemDB.LippoMaster
{
    [Table("MS_ReportOthersType")]
    public class MS_ReportOthersType : AuditedEntity
    {
        [ForeignKey("LK_OthersType")]
        public int othersTypeID { get; set; }
        public virtual LK_OthersType LK_OthersType { get; set; }

        [ForeignKey("MS_ReportType")]
        public int reportTypeID { get; set; }
        public virtual MS_ReportType MS_ReportType { get; set; }

        public bool isDefault { get; set; }
    }
}
