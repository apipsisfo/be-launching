﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace VDI.Demo.PropertySystemDB.LippoMaster
{
    public class SYS_LetterNoCounter : AuditedEntity
    {
        public int entityID { get; set; }

        [Required]
        [StringLength(6)]
        public string projectCode { get; set; }

        [Required]
        [StringLength(6)]
        public string letterType { get; set; }

        [Required]
        [StringLength(6)]
        public string month { get; set; }

        [Required]
        [StringLength(6)]
        public string year { get; set; }
        
        [Required]
        [StringLength(20)]
        public string runningNumber { get; set; }

    }
}
