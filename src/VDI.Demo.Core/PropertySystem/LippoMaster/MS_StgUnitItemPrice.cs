﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.PropertySystemDB.LippoMaster
{
    public class MS_StgUnitItemPrice : AuditedEntity
    {
        public int unitItemPriceID { get; set; }

        public decimal grossprice { get; set; }

        public DateTime tglUpdate { get; set; }

        public bool? isUpdate { get; set; }
    }
}
