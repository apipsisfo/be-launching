﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VDI.Demo.PropertySystemDB.LippoMaster
{
    [Table("TR_Addendum")]
    public class TR_Addendum : AuditedEntity
    {
        [ForeignKey("TR_BookingDocument")]
        public int bookingDocumentID { get; set; }
        public virtual TR_BookingDocument TR_BookingDocument { get; set; }

        public int entityID { get; set; }

        public int docID { get; set; }

        [Required]
        [StringLength(50)]
        public string docNoAdd { get; set; }

        public string remarks { get; set; }

        public DateTime? signedDocumentDateAdd { get; set; }

        public DateTime? signatureDateAdd { get; set; }

        public string signedDocumentFileAdd { get; set; }

        public bool status { get; set; }


    }
}
