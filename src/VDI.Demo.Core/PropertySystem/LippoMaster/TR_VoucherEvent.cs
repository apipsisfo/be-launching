﻿using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VDI.Demo.PropertySystemDB.LippoMaster
{
    public class TR_VoucherEvent : Entity
    {
        [NotMapped]
        public override int Id
        {
            get
            {
                return voucherEventId;
            }
            set { /* nothing */ }
        }

        [Key]
        public int voucherEventId { get; set; }

        public int projectID { get; set; }

        [Required]
        [StringLength(6)]
        public string voucherTypeCode { get; set; }

        [Required]
        [StringLength(50)]
        public string voucherCode { get; set; }

        [Required]
        [StringLength(6)]
        public string voucherEventTypeCode { get; set; }

        public DateTime inputTime { get; set; }

        [Required]
        public string inputUN { get; set; }
        
    }
}
