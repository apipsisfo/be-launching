﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VDI.Demo.PropertySystemDB.LippoMaster
{
    [Table("TR_ChangeUnitDetail")]
    public class TR_ChangeUnitDetail : AuditedEntity
    {

        [ForeignKey("MS_ChangeUnitParameter")]
        public int changeUnitParamId { get; set; }
        public virtual MS_ChangeUnitParameter MS_ChangeUnitParameter { get; set; }

        [ForeignKey("TR_ChangeUnit")]
        public int changeUnitId { get; set; }
        public virtual TR_ChangeUnit TR_ChangeUnit { get; set; }

        [Column(TypeName = "money")]
        public decimal value { get; set; }
    }
}
