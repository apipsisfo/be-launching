﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VDI.Demo.PropertySystemDB.LippoMaster
{
    [Table("TR_SSPMain")]
    public class TR_SSPMain : AuditedEntity
    { 
        [Required]
        [StringLength(50)]
        public string SSPID { get; set; }

        [Required]
        [StringLength(20)]
        public string unitCode { get; set; }

        [Required]
        [StringLength(8)]
        public string unitNo { get; set; }

        [Required]
        [StringLength(5)]
        public string coCode { get; set; }

        public int monthPeriod { get; set; }

        public int yearPeriod { get; set; }

        [Column("modifTime")]
        public override DateTime? LastModificationTime { get; set; }

        [Column("modifUN")]
        public override long? LastModifierUserId { get; set; }

        [Column("inputTime")]
        public override DateTime CreationTime { get; set; }

        [Column("inputUN")]
        public override long? CreatorUserId { get; set; }

        public virtual ICollection<TR_SSPDetailPayment> TR_SSPDetailPayment { get; set; }
    }
}
