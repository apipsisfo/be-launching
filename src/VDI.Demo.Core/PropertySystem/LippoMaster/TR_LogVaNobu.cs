﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VDI.Demo.PropertySystemDB.LippoMaster
{
    [Table("TR_LogVaNobu")]
    public class TR_LogVaNobu : AuditedEntity
    {
        [Required]
        [StringLength(100)]
        public string orderCode { get; set; }

        [Required]
        [StringLength(100)]
        public string VA_BankAccNo { get; set; }


        [Required]
        public string paramRequest { get; set; }

        public bool? isRequested { get; set; }
        [StringLength(100)]
        public string responseCode { get; set; }
        [StringLength(100)]
        public string errorCode { get; set; }

        public string responseData { get; set; }
    }
}
