﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using VDI.Demo.PropertySystemDB.MasterPlan.Project;

namespace VDI.Demo.PropertySystemDB.LippoMaster
{
    public class MS_CampaignHeader : AuditedEntity
    {
        [ForeignKey("MS_Project")]
        public int projectID { get; set; }
        public virtual MS_Project MS_Project { get; set; }

        [ForeignKey("MS_Program")]
        public int programID { get; set; }
        public virtual MS_Program MS_Program { get; set; }
        
        [Required]
        public string campaignName { get; set; }

        public DateTime startPeriod { get; set; }

        public DateTime endPeriod { get; set; }

        public ICollection<MS_CampaignDetail> MS_CampaignDetail { get; set; }
    }
}
