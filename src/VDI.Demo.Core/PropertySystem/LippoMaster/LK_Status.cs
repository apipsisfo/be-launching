﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VDI.Demo.PropertySystemDB.LippoMaster
{
    [Table("LK_Status")]
    public class LK_Status : AuditedEntity
    {
        public int entityID { get; set; }

        [Required]
        [StringLength(1)]
        public string statusCode { get; set; }

        [Required]
        [StringLength(20)]
        public string statusDesc { get; set; }

        public bool isOR { get; set; }
        public bool isClearingGroup { get; set; }
        public bool isOthersGroup { get; set; }
        public bool isTTBGGroup { get; set; }
        public ICollection<TR_InventoryDetail> TR_InventoryDetail { get; set; }

    }
}
