﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VDI.Demo.PropertySystemDB.LippoMaster
{
    [Table("TR_ChangeUnitDetailDoc")]
    public class TR_ChangeUnitDetailDoc : AuditedEntity
    {

        [ForeignKey("TR_ChangeUnit")]
        public int changeUnitId { get; set; }
        public virtual TR_ChangeUnit TR_ChangeUnit { get; set; }

        [Column(TypeName = "image")]
        public byte[] documentBinary { get; set; }

        [StringLength(100)]
        public string fileName { get; set; }
    }
}
