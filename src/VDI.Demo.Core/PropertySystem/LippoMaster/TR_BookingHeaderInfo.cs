﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VDI.Demo.PropertySystemDB.LippoMaster
{
    [Table("TR_BookingHeaderInfo")]
    public class TR_BookingHeaderInfo : AuditedEntity
    {
        [Column("bookingHeaderID")]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public override int Id { get; set; }

        public int? penaltyBaseRate { get; set; }
        
        public int? penaltyCalcTypeID { get; set; }

        public double? penaltyRate { get; set; }

        [StringLength(8)]
        public string penaltyFreq { get; set; }


        public DateTime? handOverDate { get; set; }

        public double? handOverMonth { get; set; }

        public double? gracePeriodMonth { get; set; }

        public double? pctCapCompensation { get; set; }

        public double? maxCompensationAmt { get; set; }
    }
}
