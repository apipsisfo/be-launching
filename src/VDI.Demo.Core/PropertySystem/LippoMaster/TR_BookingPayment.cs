﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using VDI.Demo.PropertySystemDB.OnlineBooking.PropertySystem;

namespace VDI.Demo.PropertySystemDB.LippoMaster
{
    public class TR_BookingPayment : AuditedEntity
    {
        [ForeignKey("TR_UnitOrderHeader")]
        public int unitOrderID { get; set; }
        public virtual TR_UnitOrderHeader TR_UnitOrderHeader { get; set; }

        public string bankAccName { get; set; }

        public string bankAccNo { get; set; }

        public int accountID { get; set; }

        public DateTime? clearDate { get; set; }

        public string docFile { get; set; }

        public decimal paymentAmt { get; set; }

        public DateTime paymentDate { get; set; }

        public int paymentTypeID { get; set; }

        public int? bankID { get; set; }
    }
}
