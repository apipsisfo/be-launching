﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VDI.Demo.PropertySystemDB.LippoMaster
{
    [Table("TR_TaxNotaRetur")]
    public class TR_TaxNotaRetur : AuditedEntity
    {
        [Required]
        [StringLength(50)]
        public string FPCode { get; set; }

        [Required]
        [StringLength(50)]
        public string NotaRetur { get; set; }
    }
}
