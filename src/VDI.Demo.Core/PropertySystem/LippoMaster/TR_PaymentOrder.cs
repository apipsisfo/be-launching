﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using VDI.Demo.PropertySystemDB.OnlineBooking.PPOnline;
using VDI.Demo.PropertySystemDB.OnlineBooking.PropertySystem;

namespace VDI.Demo.PropertySystemDB.LippoMaster
{
    public class TR_PaymentOrder : AuditedEntity
    {
        [StringLength(20)]
        public string orderCode { get; set; }

        public DateTime orderDate { get; set; }


        [ForeignKey("TR_BookingHeader")]
        public int bookingHeaderID { get; set; }
        public virtual TR_BookingHeader TR_BookingHeader { get; set; }

        [ForeignKey("LK_BookingOnlineStatus")]
        public int statusID { get; set; }
        public virtual LK_BookingOnlineStatus LK_BookingOnlineStatus { get; set; }

        [Column(TypeName = "money")]
        public decimal amount { get; set; }


        [ForeignKey("LK_PaymentType")]
        public int paymentTypeID { get; set; }
        public virtual LK_PaymentType LK_PaymentType { get; set; }

        [StringLength(100)]
        public string paymentServer { get; set; }

    }
}
