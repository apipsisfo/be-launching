﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VDI.Demo.PropertySystemDB.LippoMaster
{
    public class MS_StatusRoles : AuditedEntity
    {
        [StringLength(1)]
        public string entityCode { get; set; }
        
        [StringLength(50)]
        public string userName { get; set; }
        
        [StringLength(1)]
        public string statusFrom { get; set; }
        
        [StringLength(1)]
        public string statusTo { get; set; }

        public long? userID { get; set; }
    }
}
