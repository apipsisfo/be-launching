﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VDI.Demo.PropertySystemDB.CreditManagement
{
    [Table("TR_SubsidyDetail")]
    public class TR_SubsidyDetail : AuditedEntity
    {
        [ForeignKey("TR_AgreementDetail")]
        public int agreementDetailID { get; set; }
        public virtual TR_AgreementDetail TR_AgreementDetail { get; set; }

        public int? subsidyType { get; set; }
        public decimal? subsidyValue { get; set; }
        [StringLength(100)]
        public string remarks { get; set; }
        public bool isCalculateOnSI { get; set; }
    }
}
