﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using Abp.Domain.Entities.Auditing;
using VDI.Demo.PropertySystemDB.LippoMaster;

namespace VDI.Demo.PropertySystemDB.CreditManagement
{
    [Table("TR_Bilyet")]
    public class TR_Bilyet : AuditedEntity
    {
        [ForeignKey("TR_SLIKCheckingDetail")]
        public int slikCheckingDetailID { get; set; }
        public virtual TR_SLIKCheckingDetail TR_SLIKCheckingDetail { get; set; }

        [ForeignKey("TR_RetentionDetail")]
        public int retentionDetailID { get; set; }
        public virtual TR_RetentionDetail TR_RetentionDetail { get; set; }

        [StringLength(15)]
        public string bilyetNo { get; set; }
        
        [StringLength(200)]
        public string bilyetDocumentName { get; set; }
        
        [StringLength(350)]
        public string bilyetDocumentUrl { get; set; }

        public DateTime? dueDate { get; set; }

        public DateTime? dueDatePayment { get; set; }

        public bool? isActive { get; set; }

        [Required]
        public string userTypeCreation { get; set; }

        [Required]
        public string userTypeModification { get; set; }
    }
}
