﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using VDI.Demo.PropertySystemDB.MasterPlan.Unit;

namespace VDI.Demo.PropertySystemDB.CreditManagement
{
    [Table("TR_AgreementDetail")]
    public class TR_AgreementDetail : AuditedEntity
    {
        [ForeignKey("TR_CreditAgreement")]
        public int creditAgreementID { get; set; }
        public virtual TR_CreditAgreement TR_CreditAgreement { get; set; }

        [ForeignKey("MS_Cluster")]
        public int clusterID { get; set; }
        public virtual MS_Cluster MS_Cluster { get; set; }

        public int programNo { get; set; }
        [Required]
        [StringLength(100)]
        public string programName { get; set; }
        public DateTime programStart { get; set; }
        public DateTime programEnd { get; set; }
        public decimal bankRate { get; set; }
        public int? provisiFeeType { get; set; }
        public decimal? provisiValue { get; set; }
        public int? adminFeeType { get; set; }
        public decimal? adminFeeValue { get; set; }
        [StringLength(35)]
        public string addendumNo { get; set; }
        [StringLength(200)]
        public string addendumDocsName { get; set; }
        public string addendumDocsUrl { get; set; }
        public bool isActive { get; set; }
    }
}
