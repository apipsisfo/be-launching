﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VDI.Demo.PropertySystemDB.CreditManagement
{
    [Table("MS_TenorParameter")]
    public class MS_TenorParameter : AuditedEntity
    {
        public int number { get; set; }

        public virtual ICollection<TR_OfferingLetter> TR_OfferingLetter { get; set; }
    }
}
