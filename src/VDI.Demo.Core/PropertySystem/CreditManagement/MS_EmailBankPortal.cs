﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using VDI.Demo.PropertySystemDB.MasterPlan.Project;

namespace VDI.Demo.PropertySystemDB.CreditManagement
{
    [Table("MS_EmailBankPortal")]
    public class MS_EmailBankPortal : AuditedEntity
    {
        [Required]
        [StringLength(100)]
        public string from { get; set; }

        [Required]
        [StringLength(100)]
        public string to { get; set; }

        [Required]
        public string cc { get; set; }

        [ForeignKey("MS_UserAccountBank")]
        public int userAccountBankID { get; set; }
        public virtual MS_UserAccountBank MS_UserAccountBank { get; set; }

        [ForeignKey("MS_Project")]
        public int projectID { get; set; }
        public virtual MS_Project MS_Project { get; set; }

    }
}
