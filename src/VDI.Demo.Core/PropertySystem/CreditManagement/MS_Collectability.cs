﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VDI.Demo.PropertySystemDB.CreditManagement
{
    [Table("MS_Collectability")]
    public class MS_Collectability : AuditedEntity
    {
        [Required]
        [StringLength(30)]
        public string description { get; set; }

        public virtual ICollection<TR_SLIKCheckingDetail> TR_SLIKCheckingDetail { get; set; }
    }
}
