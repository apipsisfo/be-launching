﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using Abp.Domain.Entities.Auditing;

namespace VDI.Demo.PropertySystemDB.CreditManagement
{
    [Table("TR_SLIKChecking")]
    public class TR_SLIKChecking : AuditedEntity
    {
        [Required]
        public string psCode{ get; set; }

        [StringLength(50)]
        public string key { get; set; }

        [Required]
        public string UserType { get; set; }

        public virtual ICollection<TR_SLIKCheckingDetail> TR_SLIKCheckingDetail { get; set; }
    }
}
