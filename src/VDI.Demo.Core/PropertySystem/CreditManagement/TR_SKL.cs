﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using Abp.Domain.Entities.Auditing;
using VDI.Demo.PropertySystemDB.LippoMaster;

namespace VDI.Demo.PropertySystemDB.CreditManagement
{
    [Table("TR_SKL")]
    public class TR_SKL : AuditedEntity
    {
        [ForeignKey("TR_OfferingLetter")]
        public int offeringLetterID { get; set; }
        public virtual TR_OfferingLetter TR_OfferingLetter { get; set; }

        [ForeignKey("MS_GenerateSetting")]
        public int? generateSetting { get; set; }
        public virtual MS_GenerateSetting MS_GenerateSetting { get; set; }
        
        [StringLength(200)]
        public string SKLDocName { get; set; }
        
        [StringLength(350)]
        public string SKLDocUrl { get; set; }
        
        [StringLength(10)]
        public string statusGenerate { get; set; }

        public DateTime? dateGenerate { get; set; }
        
        [StringLength(100)]
        public string message { get; set; }

        public bool? isActive { get; set; }

        [StringLength(50)]
        public string runningNumber { get; set; }

        public virtual ICollection<TR_SKLDetailManual> TR_SKLDetailManual { get; set; }
    }
}
