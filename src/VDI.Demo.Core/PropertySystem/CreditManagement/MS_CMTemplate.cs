﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using VDI.Demo.PropertySystemDB.LippoMaster;
using VDI.Demo.PropertySystemDB.MasterPlan.Project;
using VDI.Demo.PropertySystemDB.MasterPlan.Unit;

namespace VDI.Demo.PropertySystemDB.CreditManagement
{
    public class MS_CMTemplate : AuditedEntity
    {
        public int entityID { get; set; }

        [ForeignKey("MS_Document")]
        public int docID { get; set; }
        public virtual MS_DocumentPS MS_Document { get; set; }

        [ForeignKey("MS_Project")]
        public int projectID { get; set; }
        public virtual MS_Project MS_Project { get; set; }

        [ForeignKey("MS_Cluster")]
        public int clusterID { get; set; }
        public virtual MS_Cluster MS_Cluster { get; set; }

        [ForeignKey("TR_BankDetail")]
        public int bankDetailID { get; set; }
        public virtual TR_BankDetail TR_BankDetail { get; set; }

        [Required]
        [StringLength(100)]
        public string templateName { get; set; }

        [Required]
        [StringLength(10)]
        public string templateCode { get; set; }

        [Required]
        public string templateFile { get; set; }

        public string gracePeriode { get; set; }

        public string handoverPeriode { get; set; }

        public bool? isActive { get; set; }
    }
}
