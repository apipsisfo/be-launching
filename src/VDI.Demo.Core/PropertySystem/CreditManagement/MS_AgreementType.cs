﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VDI.Demo.PropertySystemDB.CreditManagement
{
    [Table("MS_AgreementType")]
    public class MS_AgreementType : AuditedEntity
    {
        [Required]
        [StringLength(200)]
        public string agreementTypeName { get; set; }
        public bool isActive { get; set; }

        public ICollection<TR_CreditAgreement> TR_CreditAgreement { get; set; }
    }
}
