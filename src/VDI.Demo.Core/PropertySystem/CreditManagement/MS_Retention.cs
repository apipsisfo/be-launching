﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VDI.Demo.PropertySystemDB.CreditManagement
{
    [Table("MS_Retention")]
    public class MS_Retention : AuditedEntity
    {
        [Required]
        [StringLength(8)]
        public string retentionCode { get; set; }
        [Required]
        public string retentionName { get; set; }
        public bool isActive { get; set; }
    }
}
