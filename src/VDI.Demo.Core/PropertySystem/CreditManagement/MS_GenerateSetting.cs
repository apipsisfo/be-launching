﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VDI.Demo.PropertySystemDB.CreditManagement
{
    [Table("MS_GenerateSetting")]
    public class MS_GenerateSetting : AuditedEntity
    {
        [Required]
        [StringLength(20)]
        public string description { get; set; }

        public virtual ICollection<TR_SKL> TR_SKL { get; set; }
    }
}
