﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VDI.Demo.PersonalsDB
{
    [Table("MS_Event")]
    public class MS_Event : AuditedEntity
    {
        [StringLength(50)]
        public string eventName { get; set; }
    }
}
