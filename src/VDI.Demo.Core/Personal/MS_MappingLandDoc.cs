﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VDI.Demo.PersonalsDB
{
    [Table("MS_MappingLandDoc")]
    public class MS_MappingLandDoc : AuditedEntity<string>
    {
        [NotMapped]
        public override string Id
        {
            get
            {
                return documentType;
            }
            set { /* nothing */ }
        }

        [Key]
        [StringLength(25)]
        public string documentType { get; set; }

        public bool? KPR { get; set; }

        public bool? nonKPR { get; set; }

        public bool isActive { get; set; }
    }
}
