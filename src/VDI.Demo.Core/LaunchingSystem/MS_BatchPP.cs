﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using Abp.Domain.Entities.Auditing;

namespace VDI.Demo.LaunchingSystem
{
    public class MS_BatchPP : AuditedEntity
    {
        [Key]
        [Column(Order = 0)]
        public int batchId { get; set; }
        
        [ForeignKey("batchId")]
        public virtual MS_Batch batch { get; set; }

        [Key]
        [Column(Order = 1)]
        [Required]
        public string ppNo { get; set; }
    }
}
