﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using Abp.Domain.Entities.Auditing;

namespace VDI.Demo.LaunchingSystem
{
    public class MS_ProjectLaunching : FullAuditedEntity<int>
    {
        public int projectId { get; set; }

        public string projectName { get; set; }

        public string projectInfoId { get; set; }
        
        [Required]
        [MaxLength(100)]
        public string name { get; set; }
        
        public DateTime launchingStartDate { get; set; }

        public DateTime launchingEndDate { get; set; }
    }
}
