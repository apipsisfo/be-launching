﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using Abp.Domain.Entities.Auditing;

namespace VDI.Demo.LaunchingSystem
{
    public class MS_NumberFormat : AuditedEntity
    {
        [Required]
        [MaxLength(50)]
        public string formatType { get; set; } //PRIORITY, VIP

        [MaxLength(10)]
        public string prefix { get; set; }

        [MaxLength(10)]
        public string postfix { get; set; }

        public int lastNo { get; set; }

        public int countDigitNo { get; set; }
    }
}
