﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using Abp.Domain.Entities.Auditing;

namespace VDI.Demo.LaunchingSystem
{
    public class TR_CustomerUnitPP : FullAuditedEntity
    {
        [Key]
        [Column(Order = 0)]
        public int customerUnitId { get; set; }

        [ForeignKey("customerUnitId")]
        public TR_CustomerUnit customerUnit { get; set; }

        [Key]
        [Column(Order = 1)]
        [Required]
        [MaxLength(10)]
        public string ppNo { get; set; }
        
        public bool isPriorityLine { get; set; }
        
        public int batchId { get; set; }

        public PPStatus ppStatus { get; set; }

        public DateTime? statusDate { get; set; }

        [ForeignKey("batchId")]
        public MS_Batch msBatches { get; set; }
    }
}
