﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using Abp.Domain.Entities.Auditing;

namespace VDI.Demo.LaunchingSystem
{
    public class TR_PPQueue : AuditedEntity<long>
    {
        [Required]
        [MaxLength(10)]
        public string ppNo { get; set; }

        [Required]
        [MaxLength(10)]
        public string queueNo { get; set; }

        public PPQueueStatus queueStatus { get; set; }
    }
}
