﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using Abp.Domain.Entities.Auditing;
using VDI.Demo.PersonalsDB;

namespace VDI.Demo.LaunchingSystem
{
    public class TR_Customer : FullAuditedEntity<long>
    {
        [Required]
        public string psCode { get; set; }

        [Required]
        [MaxLength(500)]
        public string fullName { get; set; }

        [Required]
        [MaxLength(30)]
        public string idCardNo { get; set; }

        [MaxLength(4)]
        public string PIN { get; set; }

        [MaxLength(1000)]
        public string corresAddress { get; set; }

        [Required]
        public string phoneNo { get; set; }

        [Required]
        public string emailAddress { get; set; }

        public bool isDocumentComplete { get; set; }

        [MaxLength(3)]
        public string occId { get; set; }

        public virtual ICollection<TR_CustomerDocument> customerDocuments { get; set; }
        public virtual ICollection<TR_CustomerUnitPP> customerPPs { get; set; }
    }
}
