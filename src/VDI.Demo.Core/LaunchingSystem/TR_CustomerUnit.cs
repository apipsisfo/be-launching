﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using Abp.Domain.Entities.Auditing;

namespace VDI.Demo.LaunchingSystem
{
    public class TR_CustomerUnit : FullAuditedEntity<int>
    {
        public long customerId { get; set; }

        [ForeignKey("customerId")]
        public TR_Customer customer { get; set; }

        [Required]
        public string psCode { get; set; }

        [Required]
        public string orderCode { get; set; }

        public string scmCode { get; set; }

        public string memberCode { get; set; }

        public string memberName { get; set; }
        public int unitTypeId { get; set; }
        public string unitType { get; set; }        

        public int projectId { get; set; }

        public virtual ICollection<TR_CustomerUnitPP> customerUnitPPs { get; set; }

    }
}
