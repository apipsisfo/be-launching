﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using Abp.Domain.Entities.Auditing;

namespace VDI.Demo.LaunchingSystem
{
    public class MS_Batch : FullAuditedEntity
    {
        [Required]
        [MaxLength(10)]
        public string batchName { get; set; }

        public DateTime startTime { get; set; }

        public DateTime endTime { get; set; }
    }
}
