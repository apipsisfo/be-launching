﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using Abp.Domain.Entities.Auditing;

namespace VDI.Demo.LaunchingSystem
{
    public class TR_CustomerDocument : AuditedEntity<long>
    {
        public long customerId { get; set; }

        [ForeignKey("customerId")]
        public TR_Customer customer { get; set; }

        [Required]
        public string psCode { get; set; }

        [Required]
        [MaxLength(10)]
        public string documentType { get; set; } //from DocumentTypeStruct      

        [Required]
        [Column(TypeName = "image")]
        public byte[] documentBinary { get; set; }
    }
}
