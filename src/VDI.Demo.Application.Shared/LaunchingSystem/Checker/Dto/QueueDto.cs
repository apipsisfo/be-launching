﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.LaunchingSystem.Checker.Dto
{
    public class QueueDto
    {
        public string ppNo { get; set; }
        public string queueNo { get; set; }
        public int queueStatus { get; set; }
    }
}
