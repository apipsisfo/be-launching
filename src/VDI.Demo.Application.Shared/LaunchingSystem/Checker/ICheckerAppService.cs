﻿using Abp.Application.Services;
using System;
using System.Collections.Generic;
using System.Text;
using VDI.Demo.LaunchingSystem.Checker.Dto;

namespace VDI.Demo.LaunchingSystem.Checker
{
    public interface ICheckerAppService : IApplicationService
    {
       void CheckPost(QueueDto Data);
    }
}
