﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.LaunchingSystem.Customer.Dto
{
    public class CustomerDto
    {
        public int ProjectID { get; set; }
        public string ProjectName { get; set; }
        public string PsCode { get; set; }
        public string Name { get; set; }
        public string NIK { get; set; }
        public string PIN { get; set; }
        public string Address { get; set; }
        public string PhoneNo { get; set; }
        public string Email { get; set; }  
        public bool IsCompleteDoc { get; set; }
        public List<DocList> ListDoc { get; set; }
        public List<NUPList> ListNUP { get; set; }
    } 

    public class NUPList
    {
        public int No { get; set; }
        public int ppOrderID { get; set; }
        public string PIN { get; set; }
        public int detailID { get; set; }
        public string detailName { get; set; }
        public string termCode { get; set; }
        public string schemaCode { get; set; }
        public bool isPriorityLine { get; set; }
        public string memberCode { get; set; }
        public string memberName { get; set; }
        public string PPNo { get; set; }        
    }

    public class DocList
    {
        public string DocumentType { get; set; }
        public string DocumentPICType { get; set; }
        public string Base64Image { get; set; }
    }
}
