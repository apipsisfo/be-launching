﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.LaunchingSystem.Customer.Dto
{
    public class BatchListDto
    {
        public List<BatchDto> BatchLists { get; set; }
    }
    public class BatchDto
    {
        public long ID { get; set; }
        public string BatchName { get; set; }
        public string startTime { get; set; }
        public string endTime { get; set; }
        public string BatchTime { get; set; }
    }
}
