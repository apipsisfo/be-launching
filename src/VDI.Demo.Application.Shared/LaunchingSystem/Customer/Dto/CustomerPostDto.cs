﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.LaunchingSystem.Customer.Dto
{
    public class CustomerPostDto
    {
        public string psCode { get; set; }
        public string fullName { get; set; }
        public string idCardNo { get; set; }
        public string corresAddress { get; set; }   
        public string phoneNo { get; set; }        
        public string emailAddress { get; set; }
        public bool isDocumentComplete { get; set; }
        public List<CustomerDocumentPostDto> CustomerDocument { get; set; }
        public List<CustomerUnitDto> CustomerUnit { get; set; }
    }

    public class CustomerDocumentPostDto
    {
        public string documentType { get; set; } 
        public string documentBinary { get; set; }
    }

    public class CustomerUnitDto
    {        
        public string scmCode { get; set; }
        public string memberCode     { get; set; }
        public string memberName { get; set; }
        public int unitTypeId { get; set; }
        public string unitType { get; set; }
        public int projectId { get; set; }
        public string projectName { get; set; }
        public string ppNo { get; set; }
        public bool isPriorityLine { get; set; }        
        public int ppStatus { get; set; }
        public DateTime? statusDate { get; set; }
        public int bacthID { get; set; }
        public bool isChecked { get; set; }
        //public List<CustomerUnitPPDto> CustomerUnitPP { get; set; }
    }

    public class CustomerUnitPPDto
    {
        public string ppNo { get; set; }

        public bool isPriorityLine { get; set; }

        public int batchId { get; set; }

        public PPStatus ppStatus { get; set; }

        public DateTime? statusDate { get; set; }

        public int bacthID { get; set; }
    }
}
