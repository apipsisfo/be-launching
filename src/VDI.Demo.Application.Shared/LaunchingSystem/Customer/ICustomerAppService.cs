﻿using Abp.Application.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using VDI.Demo.LaunchingSystem.Customer.Dto;
using VDI.Demo.Personal.Occupation.Dto;

namespace VDI.Demo.LaunchingSystem.Customer
{
    public interface ICustomerAppService : IApplicationService
    {
        CustomerDto GetListCustomer(string NUP);
        BatchListDto GetBatchDropdown();
        Task<List<OccupationDropdownDto>> GetDropdownOccupation();
    }
}
