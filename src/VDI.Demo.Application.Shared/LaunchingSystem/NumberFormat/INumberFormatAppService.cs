﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Abp.Application.Services;

namespace VDI.Demo.LaunchingSystem.NumberFormat
{
    public interface INumberFormatAppService : IApplicationService
    {
        Task<string> GetGenerateNo(string type);
    }
}
