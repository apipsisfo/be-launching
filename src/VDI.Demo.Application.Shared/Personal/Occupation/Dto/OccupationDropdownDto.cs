﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.Personal.Occupation.Dto
{
    public class OccupationDropdownDto
    {
        public string occID { get; set; }

        public string occDesc { get; set; }
    }
}
