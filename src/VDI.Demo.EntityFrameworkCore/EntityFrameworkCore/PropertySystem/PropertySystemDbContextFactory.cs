﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using VDI.Demo.Configuration;
using VDI.Demo.Web;

namespace VDI.Demo.EntityFrameworkCore.PropertySystem
{
    public class PropertySystemDbContextFactory
    {
        public PropertySystemDbContext CreateDbContext(string[] args)
        {
            var builder = new DbContextOptionsBuilder<PropertySystemDbContext>();
            var configuration = AppConfigurations.Get(WebContentDirectoryFinder.CalculateContentRootFolder(), addUserSecrets: true);

            var propSysDbConn = configuration.GetConnectionString(DemoConsts.ConnectionStringPersonalsNewDbContext);

            PropertySystemDbContextConfigurer.Configure(builder, propSysDbConn);

            return new PropertySystemDbContext(builder.Options);
        }
    }
}
