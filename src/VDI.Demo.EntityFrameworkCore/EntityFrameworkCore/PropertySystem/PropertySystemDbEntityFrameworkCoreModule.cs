﻿using Abp.Configuration.Startup;
using Abp.Dependency;
using Abp.EntityFrameworkCore.Configuration;
using Abp.EntityFrameworkCore.Uow;
using Abp.IdentityServer4;
using Abp.Modules;
using Abp.Reflection.Extensions;
using Abp.Zero.EntityFrameworkCore;
using VDI.Demo.Configuration;

namespace VDI.Demo.EntityFrameworkCore.PropertySystem
{
    [DependsOn(
       typeof(AbpZeroCoreEntityFrameworkCoreModule),
       typeof(DemoCoreModule),
       typeof(AbpZeroCoreIdentityServerEntityFrameworkCoreModule)
       )]
    public class PropertySystemDbEntityFrameworkCoreModule : AbpModule
    {        
        public bool SkipDbContextRegistration { get; set; }

        public bool SkipDbSeed { get; set; }

        public override void PreInitialize()
        {

            Configuration.ReplaceService<IEfCoreTransactionStrategy, DbContextEfCoreTransactionStrategy>(DependencyLifeStyle.Transient);
            if (!SkipDbContextRegistration)
            {
                Configuration.Modules.AbpEfCore().AddDbContext<PropertySystemDbContext>(options =>
                {

                    PropertySystemDbContextConfigurer.Configure(options.DbContextOptions, options.ConnectionString);
                    //if (options.ExistingConnection != null)
                    //{
                    //    NewCommDbContextConfigurer.Configure(options.DbContextOptions, options.ExistingConnection);
                    //}
                    //else
                    //{

                    //}
                });


            }
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(DemoEntityFrameworkCoreModule).GetAssembly());
        }

        public override void PostInitialize()
        {
            //var configurationAccessor = IocManager.Resolve<IAppConfigurationAccessor>();

            //var decryptPropSys = EncConnString.DecryptString(configurationAccessor.Configuration["ConnectionStrings:PropertySystemDbContext"]);

            //if (!SkipDbSeed && DatabaseCheckHelper.Exist(decryptPropSys))
            //{
            //    SeedHelperProperty.SeedHostDb(IocManager);
            //}
        }
    }
}

