﻿using Abp.Dependency;
using Abp.EntityFrameworkCore;
using Dapper;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using VDI.Demo.EntityFrameworkCore.PropertySystem;
using VDI.Demo.SqlExecuter;

namespace VDI.Demo.EntityFrameworkCore
{
    public class SqlExecuter : ISqlExecuter, ITransientDependency
    {
        private readonly IDbContextProvider<PropertySystemDbContext> _dbContextPropertySystem;

        public SqlExecuter(IDbContextProvider<PropertySystemDbContext> dbContextPropertySystem)
        {
            _dbContextPropertySystem = dbContextPropertySystem ?? throw new ArgumentNullException(nameof(dbContextPropertySystem));
        }

        public IReadOnlyList<T> GetFromEngin3<T>(string sql, object parameters = null, CommandType? commandType = null)
        {
            var tempDbConn = _dbContextPropertySystem.GetDbContext();
            string tempConnStr = tempDbConn.Database.GetDbConnection().ConnectionString;

            using (var conn = new SqlConnection(tempConnStr))
            {
                return conn.Query<T>(sql, parameters).ToList();
            }

        }

        public IReadOnlyList<T> GetFromPropertySystem<T>(string sql, object parameters = null, CommandType? commandType = null)
        {
            var tempDbConn = _dbContextPropertySystem.GetDbContext();
            string tempConnStr = tempDbConn.Database.GetDbConnection().ConnectionString;

            IDbTransaction transaction = null;

            using (var conn = new SqlConnection(tempConnStr))
            {
                return conn.Query<T>(sql, parameters, transaction, true, 1800).ToList();
            }
        }

        public int ExecutePropertySystem(string sql, params object[] parameters)
        {
            return _dbContextPropertySystem.GetDbContext().Database.ExecuteSqlCommand(sql, parameters);
        }
    }
}
