﻿using Abp.Configuration.Startup;
using Abp.Dependency;
using Abp.EntityFrameworkCore.Configuration;
using Abp.EntityFrameworkCore.Uow;
using Abp.IdentityServer4;
using Abp.Modules;
using Abp.Reflection.Extensions;
using Abp.Zero.EntityFrameworkCore;

namespace VDI.Demo.EntityFrameworkCore.Personal
{
    [DependsOn(
     typeof(AbpZeroCoreEntityFrameworkCoreModule),
     typeof(DemoCoreModule),
     typeof(AbpZeroCoreIdentityServerEntityFrameworkCoreModule)
     )]

    public class PersonalsNewDbEntityFrameworkCoreModule : AbpModule
    {
        public bool SkipDbContextRegistration { get; set; }

        public bool SkipDbSeed { get; set; }

        public override void PreInitialize()
        {
            Configuration.ReplaceService<IEfCoreTransactionStrategy, DbContextEfCoreTransactionStrategy>(DependencyLifeStyle.Transient);
            if (!SkipDbContextRegistration)
            {
                Configuration.Modules.AbpEfCore().AddDbContext<PersonalsNewDbContext>(options =>
                {
                    if (options.ExistingConnection != null)
                    {
                        PersonalsNewDbContextConfigurer.Configure(options.DbContextOptions, options.ExistingConnection);
                    }
                    else
                    {
                        PersonalsNewDbContextConfigurer.Configure(options.DbContextOptions, options.ConnectionString);
                    }
                });
            }
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(PersonalsNewDbEntityFrameworkCoreModule).GetAssembly());
        }

        public override void PostInitialize()
        {
            //var configurationAccessor = IocManager.Resolve<IAppConfigurationAccessor>();

            //var decryptPersonal = EncConnString.DecryptString(configurationAccessor.Configuration["ConnectionStrings:PersonalsNewDbContext"]);

            //if (!SkipDbSeed && DatabaseCheckHelper.Exist(decryptPersonal))
            //{
            //    SeedHelperPersonal.SeedHostDb(IocManager);
            //}
        }
    }
}
