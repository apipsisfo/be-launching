﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Text;
using Microsoft.EntityFrameworkCore;

namespace VDI.Demo.EntityFrameworkCore.LaunchingSystem
{
    public static class LaunchingSystemDbContextConfigurer
    {
        public static void Configure(DbContextOptionsBuilder<LaunchingSystemDbContext> builder, string connectionString)
        {
            builder.UseSqlServer(connectionString);
        }

        public static void Configure(DbContextOptionsBuilder<LaunchingSystemDbContext> builder, DbConnection connection)
        {
            builder.UseSqlServer(connection);
        }
    }
}
