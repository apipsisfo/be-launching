﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using VDI.Demo.Configuration;
using VDI.Demo.Web;

namespace VDI.Demo.EntityFrameworkCore.LaunchingSystem
{
    /* This class is needed to run "dotnet ef ..." commands from command line on development. Not used anywhere else */
    public class LaunchingSystemDbContextFactory : IDesignTimeDbContextFactory<LaunchingSystemDbContext>
    {
        public LaunchingSystemDbContext CreateDbContext(string[] args)
        {
            var builder = new DbContextOptionsBuilder<LaunchingSystemDbContext>();
            var configuration = AppConfigurations.Get(WebContentDirectoryFinder.CalculateContentRootFolder(), addUserSecrets: true);

            var launchingDbConn = configuration.GetConnectionString(DemoConsts.LaunchingSystemConnectionStringName);

            LaunchingSystemDbContextConfigurer.Configure(builder, launchingDbConn);

            return new LaunchingSystemDbContext(builder.Options);
        }
    }
}
