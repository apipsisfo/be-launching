﻿using System;
using System.Collections.Generic;
using System.Text;
using Abp.Domain.Repositories;
using Abp.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using VDI.Demo.LaunchingSystem;

namespace VDI.Demo.EntityFrameworkCore.LaunchingSystem
{
    public class LaunchingSystemDbContext : AbpDbContext
    {
        /// <summary>
        /// Put database model here
        /// </summary>
        /// <param name="options"></param>

        public virtual DbSet<TR_Customer> TR_Customer { get; set; }
        public virtual DbSet<TR_CustomerDocument> TR_CustomerDocument { get; set; }
        public virtual DbSet<TR_CustomerUnit> TR_CustomerUnit { get; set; }
        public virtual DbSet<TR_CustomerUnitPP> TR_CustomerUnitPP { get; set; }
        public virtual DbSet<TR_PPQueue> TR_PPQueue { get; set; }

        public virtual DbSet<MS_Batch> MS_Batch { get; set; }
        public virtual DbSet<MS_BatchPP> MS_BatchPP { get; set; }
        public virtual DbSet<MS_NumberFormat> MS_NumberFormat { get; set; }
        public virtual DbSet<MS_ProjectLaunching> MS_ProjectLaunching { get; set; }

        public LaunchingSystemDbContext(DbContextOptions<LaunchingSystemDbContext> options)
          : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<TR_CustomerUnitPP>().HasKey(table => new { table.customerUnitId, table.ppNo });

        }
    }
}
