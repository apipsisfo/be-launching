﻿using Abp.Configuration.Startup;
using Abp.Dependency;
using Abp.EntityFrameworkCore.Configuration;
using Abp.EntityFrameworkCore.Uow;
using Abp.IdentityServer4;
using Abp.Modules;
using Abp.Reflection.Extensions;
using Abp.Zero.EntityFrameworkCore;

namespace VDI.Demo.EntityFrameworkCore.LaunchingSystem
{
    [DependsOn(
          typeof(AbpZeroCoreEntityFrameworkCoreModule),
          typeof(DemoCoreModule),
          typeof(AbpZeroCoreIdentityServerEntityFrameworkCoreModule)
          )]
    public class LaunchingSystemEntityFrameworkCoreModule : AbpModule
    {
        public bool SkipDbContextRegistration { get; set; }

        public bool SkipDbSeed { get; set; }

        public override void PreInitialize()
        {
            Configuration.ReplaceService<IEfCoreTransactionStrategy, DbContextEfCoreTransactionStrategy>(DependencyLifeStyle.Transient);
            if (!SkipDbContextRegistration)
            {
                Configuration.Modules.AbpEfCore().AddDbContext<LaunchingSystemDbContext>(options =>
                {
                    if (options.ExistingConnection != null)
                    {
                        LaunchingSystemDbContextConfigurer.Configure(options.DbContextOptions, options.ExistingConnection);
                    }
                    else
                    {
                        LaunchingSystemDbContextConfigurer.Configure(options.DbContextOptions, options.ConnectionString);
                    }
                });
            }
        }
        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(LaunchingSystemEntityFrameworkCoreModule).GetAssembly());
        }

        public override void PostInitialize()
        {
            // Call data seeder here
        }
    }
}
