﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;

namespace VDI.Demo.Migrations
{
    internal static class DbSetExtensions
    {
        /// <summary>
        /// Tell the database to allow inserting values into IDENTITY columns.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        public static void EnableInsertingIntoIdentityColumns<T>(this DbContext dbContext)
        {
            var command = $"SET IDENTITY_INSERT [{typeof(T).Name}] ON";
            dbContext.Database.ExecuteSqlCommand(command);
        }

        /// <summary>
        /// Tell the database to forbid inserting values into IDENTITY columns.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        public static void DisableInsertingIntoIdentityColumns<T>(this DbContext dbContext)
        {
            var command = $"SET IDENTITY_INSERT [{typeof(T).Name}] OFF";
            dbContext.Database.ExecuteSqlCommand(command);
        }


        public static void AddRowsConditionallyAndSaveIdentity<T>(
            this DbContext dbContext,
            Func<DbSet<T>, T, bool> condition,
            params T[] items
        ) where T : class
        {
            AddRowsConditionallyAndSaveCore<T>(
                dbContext,
                condition,
                true,
                items
            );
        }

        public static void AddRowsConditionallyAndSave<T>(
            this DbContext dbContext,
            Func<DbSet<T>, T, bool> condition,
            params T[] items
        ) where T : class
        {
            AddRowsConditionallyAndSaveCore(
                dbContext,
                condition,
                false,
                items
            );
        }

        /// <summary>
        /// For each item in a range, check that it meets a condition before
        /// adding it to the <see cref="DbSet{TEntity}"/>. Note that this also
        /// saves changes.
        /// </summary>
        private static void AddRowsConditionallyAndSaveCore<T>(
            this DbContext dbContext,
            Func<DbSet<T>, T, bool> condition,
            bool tableHasIdentity,
            params T[] items
        ) where T : class
        {
            var matchingDbSetProp = dbContext.GetType().GetProperties()
                .FirstOrDefault(x => x.PropertyType.IsAssignableFrom(typeof(DbSet<T>)));

            if (matchingDbSetProp == null)
            {
                throw new Exception($"Could not find DbSet<{typeof(T).Name}> to add range to.");
            }

            var dbSet = (DbSet<T>)matchingDbSetProp.GetValue(dbContext);

            foreach (var item in items)
            {
                if (condition(dbSet, item))
                {
                    dbSet.Add(item);
                }
            }

            if (tableHasIdentity) dbContext.EnableInsertingIntoIdentityColumns<T>();

            dbContext.SaveChanges();

            if (tableHasIdentity) dbContext.DisableInsertingIntoIdentityColumns<T>();
        }
    }
}
