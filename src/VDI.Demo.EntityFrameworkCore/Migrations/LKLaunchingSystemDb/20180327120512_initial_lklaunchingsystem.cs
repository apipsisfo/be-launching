﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace VDI.Demo.Migrations.LKLaunchingSystemDb
{
    public partial class initial_lklaunchingsystem : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "MS_BatchEntry",
                columns: table => new
                {
                    batchSeq = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ClusterCode = table.Column<string>(maxLength: 10, nullable: true),
                    ClusterName = table.Column<string>(maxLength: 100, nullable: true),
                    FloorSize = table.Column<string>(maxLength: 50, nullable: true),
                    FloorSizeMath = table.Column<decimal>(nullable: true),
                    IsRunOut = table.Column<bool>(nullable: false),
                    batchCode = table.Column<string>(maxLength: 3, nullable: false),
                    batchMaxNum = table.Column<int>(nullable: true),
                    batchStartNum = table.Column<int>(nullable: false),
                    inputUN = table.Column<string>(maxLength: 50, nullable: false),
                    isActive = table.Column<int>(nullable: true),
                    isBookingFee = table.Column<int>(nullable: true),
                    isConvertOnly = table.Column<int>(nullable: true),
                    isSellOnly = table.Column<int>(nullable: true),
                    isTopupOnly = table.Column<int>(nullable: false),
                    maxToken = table.Column<int>(nullable: false),
                    maxTopupFromOldBatch = table.Column<int>(nullable: false),
                    minToken = table.Column<int>(nullable: false),
                    priorityPassPrice = table.Column<decimal>(type: "money", nullable: true),
                    projectCode = table.Column<string>(maxLength: 10, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MS_BatchEntry", x => x.batchSeq);
                });

            migrationBuilder.CreateTable(
                name: "TR_PriorityPass",
                columns: table => new
                {
                    PPNo = table.Column<string>(maxLength: 6, nullable: false),
                    CardNo = table.Column<string>(maxLength: 30, nullable: true),
                    KPA = table.Column<string>(maxLength: 50, nullable: true),
                    Kamar = table.Column<string>(maxLength: 50, nullable: true),
                    Lantai = table.Column<string>(maxLength: 50, nullable: true),
                    PPStatus = table.Column<string>(maxLength: 1, nullable: true),
                    PaymentType = table.Column<string>(maxLength: 20, nullable: true),
                    TableNo = table.Column<int>(nullable: true),
                    Token = table.Column<int>(nullable: true),
                    bank = table.Column<string>(maxLength: 20, nullable: true),
                    batchSeq = table.Column<int>(nullable: false),
                    buyDate = table.Column<DateTime>(nullable: false),
                    dealingTime = table.Column<DateTime>(nullable: true),
                    idCard = table.Column<byte[]>(type: "image", nullable: true),
                    idSetting = table.Column<int>(nullable: true),
                    inputFrom = table.Column<string>(maxLength: 50, nullable: true),
                    inputTime = table.Column<DateTime>(nullable: false),
                    inputUN = table.Column<string>(maxLength: 50, nullable: true),
                    memberCode = table.Column<string>(maxLength: 12, nullable: true),
                    oldPPNo = table.Column<string>(maxLength: 6, nullable: true),
                    psCode = table.Column<string>(maxLength: 8, nullable: true),
                    regTime = table.Column<DateTime>(nullable: true),
                    remarks = table.Column<string>(maxLength: 300, nullable: true),
                    scmCode = table.Column<string>(maxLength: 3, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TR_PriorityPass", x => x.PPNo);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "MS_BatchEntry");

            migrationBuilder.DropTable(
                name: "TR_PriorityPass");
        }
    }
}
