﻿namespace VDI.Demo
{
    public class DemoConsts
    {
        public const string LocalizationSourceName = "Demo";

        public const string ConnectionStringName = "Default";

        public const string LaunchingSystemConnectionStringName = "LaunchingSystemDb";

        public const string ConnectionStringPersonalsNewDbContext = "PersonalsNewDbContext";

        public const string ConnectionStringPropertySystemDbContext = "PropertySystemDbContext";

        public const bool MultiTenancyEnabled = false;

        public const int PaymentCacheDurationInMinutes = 30;
    }

    public struct DocumentTypeStruct
    {
        public const string KTP = "KTP";
        public const string NPWP = "NPWP";
        public const string KITAS = "KITAS";
    }

    public struct NumberingTypeStruct
    {
        public const string PriorityArea = "PRIORITY";
        public const string VIPArea = "VIP";
    }

    public enum PPQueueStatus
    {
        AvailableForChecking = 0,
        Processed = 1,
        Cancelled = 2,
        Done = 3,
        Unchecked = 4
    }

    public enum PPStatus
    {
        Registered = 0,
        Checked = 1
    }
}